# -*- coding: utf-8 -*-
# vim: autoindent shiftwidth=4 expandtab textwidth=120 tabstop=4 softtabstop=4

###############################################################################
# OpenLP - Open Source Lyrics Projection                                      #
# --------------------------------------------------------------------------- #
# Copyright (c) 2008-2017 OpenLP Developers                                   #
# --------------------------------------------------------------------------- #
# This program is free software; you can redistribute it and/or modify it     #
# under the terms of the GNU General Public License as published by the Free  #
# Software Foundation; version 2 of the License.                              #
#                                                                             #
# This program is distributed in the hope that it will be useful, but WITHOUT #
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or       #
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for    #
# more details.                                                               #
#                                                                             #
# You should have received a copy of the GNU General Public License along     #
# with this program; if not, write to the Free Software Foundation, Inc., 59  #
# Temple Place, Suite 330, Boston, MA 02111-1307 USA                          #
###############################################################################

from distutils.version import LooseVersion
import os
import logging

from openlp.core.common import is_macosx

if is_macosx():
    from AppKit import NSURL, NSMutableDictionary
    from ScriptingBridge import SBApplication

from openlp.core.common import Settings
from .presentationcontroller import PresentationController, PresentationDocument

log = logging.getLogger(__name__)

KEYNOTE_ID = 'com.apple.iWork.Keynote'
KEYNOTE_MIN_VERSION = '7.0'


class KeynoteSaveOptions(object):
    """
    Keynote saving options
    """
    Yes = 0x79657320  # 'yes' Save the file.
    No = 0x6e6f2020  # 'no' Do not save the file.
    Ask = 0x61736b20  # 'ask' Ask the user whether or not to save the file.


class KeynoteExportFormat(object):
    """
    The format to use in export.
    """
    HTML = 0x4b68746d  # HTML
    QuickTimeMovie = 0x4b6d6f76  # QuickTime movie
    PDF = 0x4b706466  # PDF
    SlideImages = 0x4b696d67  # image
    MicrosoftPowerPoint = 0x4b707074  # Microsoft PowerPoint
    Keynote09 = 0x4b6b6579  # Keynote 09


class KeynoteImageExportFormats (object):
    """
    Format for resulting images when export slides.
    """
    JPEG = 0x4b69666a  # 'Kifj'
    PNG = 0x4b696670  # 'Kifp'
    TIFF = 0x4b696674  # 'Kift'


class KeynoteController(PresentationController):
    """
    Class to control interactions with KeyNote Presentations
    It creates the runtime Environment , Loads the and Closes the Presentation
    As well as triggering the correct activities based on the users input
    """
    log.info('KeynoteController loaded')

    def __init__(self, plugin):
        """
        Initialise the class
        """
        log.debug('Initialising')
        super(KeynoteController, self).__init__(plugin, 'Keynote', KeynoteDocument)
        self.supports = ['key']
        self.process = None

    def check_available(self):
        """
        KeyNote is able to run on this machine
        """
        log.debug('check_available')
        if not is_macosx():
            return False
        try:
            self.process = SBApplication.applicationWithBundleIdentifier_(KEYNOTE_ID)
            version = self.process.version()
            if LooseVersion(version.split()[0]) < LooseVersion(KEYNOTE_MIN_VERSION):
                return False
        except:
            return False
        self.kill()
        return True

    def start_process(self):
        """
        Loads KeyNote process
        """
        log.debug('start_process')
        if not self.process or not self.process.isRunning():
            self.process = SBApplication.applicationWithBundleIdentifier_(KEYNOTE_ID)
            self.apply_app_settings()

    def kill(self):
        """
        Called at system exit to clean up any running presentations
        """
        log.debug('Kill Keynote')
        while self.docs and self.process.isRunning():
            self.docs[0].close_presentation()
        if self.process is None or not self.process.isRunning():
            return
        try:
            total = self.process.documents()
            if self.process and total and len(total) > 0:
                return
            # Exit with asking for saving modified
            self.process.quitSaving_(KeynoteSaveOptions.Ask)
        except:
            log.debug('Kill Keynote failed')
        self.process = None

    def apply_app_settings(self):
        """
        Apply settings for Keynote
        PresentationModeEnableFeedbackDisplay = True if in settings of OpenLP show presenter view = True
        PresentationModeUseSecondary = 1 if OpenLP monitor for output = Screen 2
        """
        return
        openlp_settings = Settings('openlp.org', 'OpenLP')
        keynote_settings = Settings('apple', 'iWork.Keynote')
        use_secondary = int(keynote_settings.value('PresentationModeUseSecondary'))
        monitor = openlp_settings.value('core/monitor')
        override_position = openlp_settings.value('core/override position')
        if not override_position and use_secondary != monitor:
            keynote_settings.setValue('PresentationModeUseSecondary', monitor)
        elif override_position and use_secondary != 0:
            keynote_settings.setValue('PresentationModeUseSecondary', '0')
        show_presenter_view = openlp_settings.value(self.plugin.settings_section + '/show presenter view')
        keynote_feedback_display = keynote_settings.value('PresentationModeEnableFeedbackDisplay')
        if show_presenter_view != keynote_feedback_display:
            keynote_settings.setValue('PresentationModeEnableFeedbackDisplay', show_presenter_view)
        play_well_with_others = keynote_settings.value('PresentationModePlayWellWithOthers')
        if not play_well_with_others:
            keynote_settings.setValue('PresentationModePlayWellWithOthers', True)


class KeynoteDocument(PresentationDocument):
    """
    Class which holds information and controls a single presentation
    """

    def __init__(self, controller, presentation):
        """
        Constructor, store information about the file and initialise
        """
        log.debug('Init Presentation Keynote')
        super(KeynoteDocument, self).__init__(controller, presentation)
        self.presentation = None
        self.is_playing = False

    def load_presentation(self):
        """
        Called when a presentation is added to the SlideController.
        Opens the Keynote file using the process created earlier.
        """
        log.debug('load_presentation')
        if not self.controller.process or not self.controller.process.isRunning():
            self.controller.start_process()
        try:
            self.controller.process.open_(self.file_path)
        except:
            log.debug('Keynote open failed')
            return False
        documents = self.controller.process.documents()
        for document in documents:
            file_url = document.file() # get NSURL object of path
            if file_url and self.file_path == file_url.path():
                self.presentation = document
                self.create_thumbnails()
                return True
        self.presentation = None
        return False

    def create_thumbnails(self):
        """
        Create the thumbnail images for the current presentation.
        """
        log.debug('create_thumbnails')
        if self.check_thumbnails():
            return
        thumbnail_folder = self.get_thumbnail_folder()
        if not os.path.exists(thumbnail_folder):
            os.makedirs(thumbnail_folder)
        try:
            self.controller.process.open_(self.file_path)
        except:
            log.debug('KeyNote open failed')
            return
        uri_folder = NSURL.fileURLWithPath_(thumbnail_folder)
        options = NSMutableDictionary.dictionaryWithDictionary_({})
        self.presentation.exportTo_as_withProperties_(uri_folder, KeynoteExportFormat.SlideImages, options)
        slide_no = 0
        for filename in os.listdir(thumbnail_folder):
            full_filename = os.path.join(thumbnail_folder, filename)
            if not os.path.isfile(full_filename) or not filename.endswith('.png') or filename == 'icon.png':
                continue
            slide_no = slide_no + 1
            if not filename.startswith(self.controller.thumbnail_prefix):
                path = os.path.join(thumbnail_folder, self.controller.thumbnail_prefix + str(slide_no) + '.png')
                try:
                    os.rename(full_filename, path)
                except:
                    open(path, 'w').write(open(full_filename, 'r').read())
                    os.unlink(full_filename)

    def close_presentation(self):
        """
        Close presentation and clean up objects. This is triggered by a new
        object being added to SlideController or OpenLP being shut down.
        """
        log.debug('close_presentation')
        if self.presentation:
            try:
                self.presentation.closeSaving_savingIn_(KeynoteSaveOptions.No, '')
            except:
                log.debug('Could not close the presentation')
        self.presentation = None
        self.controller.remove_doc(self)

    def is_loaded(self):
        """
        Returns ``True`` if a presentation is loaded.
        """
        log.debug('is_loaded')
        try:
            if not self.controller.process.isRunning():
                return False
            windows = self.controller.process.windows()
            if len(windows) == 0:
                return False
            documents = self.controller.process.documents()
            if len(documents) == 0:
                return False
        except:
            return False
        for document in documents:
            file_url = document.file()
            if file_url and self.file_path == file_url.path():
                return True
        return False

    def is_active(self):
        """
        Returns ``True`` if a presentation is currently active.
        """
        log.debug('is_active')
        if not self.is_loaded():
            return False
        try:
            if not self.controller.process.playing():
                return False
        except:
            return False
        return True

    def unblank_screen(self):
        """
        Unblanks (restores) the presentation.
        """
        log.debug('unblank_screen')
        if self.is_blank():
            self.start_presentation()

    def blank_screen(self):
        """
        Blanks the screen.
        """
        log.debug('blank_screen')
        self.presentation.stop()

    def is_blank(self):
        """
        Returns ``True`` if screen is blank.
        """
        log.debug('is_blank')
        if self.is_active():
            return not self.controller.process.playing()
        else:
            return False

    def stop_presentation(self):
        """
        Stops the current presentation and hides the output.
        """
        log.debug('stop_presentation')
        self.presentation.stop()
        self.is_playing = False

    def start_presentation(self):
        """
        Starts a presentation from the beginning.
        """
        log.debug('start_presentation')
        if not self.is_active():
            self.controller.apply_app_settings()
            try:
                slide = self.presentation.slides()[self.slidenumber-1]
                self.presentation.setCurrentSlide_(slide)
                self.presentation.startFrom_(slide)
                self.is_playing = True
            except:
                pass

    def get_slide_number(self):
        """
        Returns the current slide number.
        """
        log.debug('get_slide_number')
        return self.presentation.currentSlide().slideNumber()

    def get_slide_count(self):
        """
        Returns total number of slides.
        """
        log.debug('get_slide_count')
        slides = self.presentation.slides()
        return len(slides)

    def goto_slide(self, slideno):
        """
        Moves to a specific slide in the presentation.
        """
        log.debug('goto_slide')
        slide = self.presentation.slides()[slideno - 1]
        self.presentation.setCurrentSlide_(slide)
        self.presentation.startFrom_(slide)

    def next_step(self):
        """
        Triggers the next effect of slide on the running presentation.
        """
        log.debug('next_step')
        self.controller.process.showNext()
        if self.get_slide_number() > self.get_slide_count():
            self.previous_step()

    def previous_step(self):
        """
        Triggers the previous slide on the running presentation.
        """
        log.debug('previous_step')
        self.controller.process.showPrevious()

    def poll_slidenumber(self, is_live, hide_mode):
        """
        Check the state of Slideshow - if it finished then set hide_mode to Screen
        Keynote does not support black screen.
        """
        super(KeynoteDocument, self).poll_slidenumber(is_live, hide_mode)
        if not self.is_active() and self.is_playing:
            log.debug('Exited from slide show')
            self.is_playing = False
            self.controller.plugin.live_controller.toggle_display('desktop')
