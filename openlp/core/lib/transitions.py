# -*- coding: utf-8 -*-
# vim: autoindent shiftwidth=4 expandtab textwidth=120 tabstop=4 softtabstop=4

###############################################################################
# OpenLP - Open Source Lyrics Projection                                      #
# --------------------------------------------------------------------------- #
# Copyright (c) 2008-2017 OpenLP Developers                                   #
# --------------------------------------------------------------------------- #
# This program is free software; you can redistribute it and/or modify it     #
# under the terms of the GNU General Public License as published by the Free  #
# Software Foundation; version 2 of the License.                              #
#                                                                             #
# This program is distributed in the hope that it will be useful, but WITHOUT #
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or       #
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for    #
# more details.                                                               #
#                                                                             #
# You should have received a copy of the GNU General Public License along     #
# with this program; if not, write to the Free Software Foundation, Inc., 59  #
# Temple Place, Suite 330, Boston, MA 02111-1307 USA                          #
###############################################################################
"""
The :mod:`transitions` module provides management functionality for a transitions
of text on main display.
"""

import logging


log = logging.getLogger(__name__)


class TransitionType(object):
    """
    Type enumeration for transitions on main display.
    """
    NoTransition = 0
    FadeOutFadeIn = 1
    CrossFade = 2
    FadeIn = 3
    MoveLeft = 4
    MoveRight = 5
    MoveUp = 6
    MoveDown = 7

    Names = ['No transition', 'Fade-out-Fade-in', 'Cross Fade', 'Fade-in', 'Move-left', 'Move-right', 'Move-Up',
             'Move-down']

    @staticmethod
    def to_string(transition_type):
        """
        Return a string representation of a transition type.
        """
        if transition_type == TransitionType.NoTransition:
            return 'NoTransition'
        elif transition_type == TransitionType.FadeOutFadeIn:
            return 'FadeOutFadeIn'
        elif transition_type == TransitionType.CrossFade:
            return 'CrossFade'
        elif transition_type == TransitionType.FadeIn:
            return 'FadeIn'
        elif transition_type == TransitionType.MoveLeft:
            return 'MoveLeft'
        elif transition_type == TransitionType.MoveRight:
            return 'MoveRight'
        elif transition_type == TransitionType.MoveUp:
            return 'MoveUp'
        elif transition_type == TransitionType.MoveDown:
            return 'MoveDown'


    @staticmethod
    def from_string(type_string):
        """
        Return a transition type for the given string.
        """
        if type_string == 'NoTransition' or type_string == 'False':
            return TransitionType.NoTransition
        elif type_string == 'FadeOutFadeIn' or type_string == 'True':
            return TransitionType.FadeOutFadeIn
        elif type_string == 'CrossFade':
            return TransitionType.CrossFade
        elif type_string == 'FadeIn':
            return TransitionType.FadeIn
        elif type_string == 'MoveLeft':
            return TransitionType.MoveLeft
        elif type_string == 'MoveRight':
            return TransitionType.MoveRight
        elif type_string == 'MoveUp':
            return TransitionType.MoveUp
        elif type_string == 'MoveDown':
            return TransitionType.MoveDown
        else:
            return TransitionType.NoTransition

    @staticmethod
    def maximum_speed(transition_type):
        """
        Return a string representation of a transition type.
        """
        if transition_type == TransitionType.NoTransition:
            return 0
        elif transition_type == TransitionType.FadeOutFadeIn:
            return 3
        elif transition_type == TransitionType.CrossFade:
            return 3
        elif transition_type == TransitionType.FadeIn:
            return 3
        elif transition_type == TransitionType.MoveLeft:
            return 3
        elif transition_type == TransitionType.MoveRight:
            return 3
        elif transition_type == TransitionType.MoveUp:
            return 3
        elif transition_type == TransitionType.MoveDown:
            return 3

    @staticmethod
    def minimum_speed(transition_type):
        """
        Return a string representation of a transition type.
        """
        if transition_type == TransitionType.NoTransition:
            return 0
        elif transition_type == TransitionType.FadeOutFadeIn:
            return -3
        elif transition_type == TransitionType.CrossFade:
            return -3
        elif transition_type == TransitionType.FadeIn:
            return -3
        elif transition_type == TransitionType.MoveLeft:
            return -3
        elif transition_type == TransitionType.MoveRight:
            return -3
        elif transition_type == TransitionType.MoveUp:
            return -3
        elif transition_type == TransitionType.MoveDown:
            return -3