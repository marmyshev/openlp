# -*- coding: utf-8 -*-
# vim: autoindent shiftwidth=4 expandtab textwidth=120 tabstop=4 softtabstop=4

###############################################################################
# OpenLP - Open Source Lyrics Projection                                      #
# --------------------------------------------------------------------------- #
# Copyright (c) 2008-2017 OpenLP Developers                                   #
# --------------------------------------------------------------------------- #
# This program is free software; you can redistribute it and/or modify it     #
# under the terms of the GNU General Public License as published by the Free  #
# Software Foundation; version 2 of the License.                              #
#                                                                             #
# This program is distributed in the hope that it will be useful, but WITHOUT #
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or       #
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for    #
# more details.                                                               #
#                                                                             #
# You should have received a copy of the GNU General Public License along     #
# with this program; if not, write to the Free Software Foundation, Inc., 59  #
# Temple Place, Suite 330, Boston, MA 02111-1307 USA                          #
###############################################################################
"""
The :mod:`~openlp.core.ui.media.vlcplayer` module contains our VLC component wrapper
"""
from datetime import datetime
from distutils.version import LooseVersion
import logging
import os
import threading
import sys
import ctypes
from PyQt5 import QtCore, QtWidgets

from openlp.core.common import Settings, is_win, is_macosx, is_linux, Registry
from openlp.core.ui import MainDisplay, HideMode
from openlp.core.lib import translate
from openlp.core.ui.media import MediaState, MediaType
from openlp.core.ui.media.mediaplayer import MediaPlayer

log = logging.getLogger(__name__)

# Audio and video extensions copied from 'include/vlc_interface.h' from vlc 2.2.0 source
AUDIO_EXT = ['*.3ga', '*.669', '*.a52', '*.aac', '*.ac3', '*.adt', '*.adts', '*.aif', '*.aifc', '*.aiff', '*.amr',
             '*.aob', '*.ape', '*.awb', '*.caf', '*.dts', '*.flac', '*.it', '*.kar', '*.m4a', '*.m4b', '*.m4p', '*.m5p',
             '*.mid', '*.mka', '*.mlp', '*.mod', '*.mpa', '*.mp1', '*.mp2', '*.mp3', '*.mpc', '*.mpga', '*.mus',
             '*.oga', '*.ogg', '*.oma', '*.opus', '*.qcp', '*.ra', '*.rmi', '*.s3m', '*.sid', '*.spx', '*.thd', '*.tta',
             '*.voc', '*.vqf', '*.w64', '*.wav', '*.wma', '*.wv', '*.xa', '*.xm']

VIDEO_EXT = ['*.3g2', '*.3gp', '*.3gp2', '*.3gpp', '*.amv', '*.asf', '*.avi', '*.bik', '*.divx', '*.drc', '*.dv',
             '*.f4v', '*.flv', '*.gvi', '*.gxf', '*.iso', '*.m1v', '*.m2v', '*.m2t', '*.m2ts', '*.m4v', '*.mkv',
             '*.mov', '*.mp2', '*.mp2v', '*.mp4', '*.mp4v', '*.mpe', '*.mpeg', '*.mpeg1', '*.mpeg2', '*.mpeg4', '*.mpg',
             '*.mpv2', '*.mts', '*.mtv', '*.mxf', '*.mxg', '*.nsv', '*.nuv', '*.ogg', '*.ogm', '*.ogv', '*.ogx', '*.ps',
             '*.rec', '*.rm', '*.rmvb', '*.rpl', '*.thp', '*.tod', '*.ts', '*.tts', '*.txd', '*.vob', '*.vro', '*.webm',
             '*.wm', '*.wmv', '*.wtv', '*.xesc',
             # These extensions was not in the official list, added manually.
             '*.nut', '*.rv', '*.xvid']


def get_vlc():
    """
    In order to make this module more testable, we have to wrap the VLC import inside a method. We do this so that we
    can mock out the VLC module entirely.

    :return: The "vlc" module, or None
    """
    if 'openlp.core.ui.media.vendor.vlc' in sys.modules:
        # If VLC has already been imported, no need to do all the stuff below again
        return sys.modules['openlp.core.ui.media.vendor.vlc']
    is_vlc_available = False
    try:
        if is_macosx():
            # Newer versions of VLC on OS X need this. See https://forum.videolan.org/viewtopic.php?t=124521
            os.environ['VLC_PLUGIN_PATH'] = '/Applications/VLC.app/Contents/MacOS/plugins'
        # On Windows when frozen in PyInstaller, we need to blank SetDllDirectoryW to allow loading of the VLC dll.
        # This is due to limitations (by desgin) in PyInstaller. SetDllDirectoryW original value is restored once
        # VLC has been imported.
        if is_win():
            buffer_size = 1024
            dll_directory = ctypes.create_unicode_buffer(buffer_size)
            new_buffer_size = ctypes.windll.kernel32.GetDllDirectoryW(buffer_size, dll_directory)
            dll_directory = ''.join(dll_directory[:new_buffer_size]).replace('\0', '')
            log.debug('Original DllDirectory: %s' % dll_directory)
            ctypes.windll.kernel32.SetDllDirectoryW(None)
        from openlp.core.ui.media.vendor import vlc
        if is_win():
            ctypes.windll.kernel32.SetDllDirectoryW(dll_directory)
        is_vlc_available = bool(vlc.get_default_instance())
    except (ImportError, NameError, NotImplementedError):
        pass
    except OSError as e:
        if is_win():
            if not isinstance(e, WindowsError) and e.winerror != 126:
                raise
        else:
            pass
    if is_vlc_available:
        try:
            VERSION = vlc.libvlc_get_version().decode('UTF-8')
        except:
            VERSION = '0.0.0'
        # LooseVersion does not work when a string contains letter and digits (e. g. 2.0.5 Twoflower).
        # http://bugs.python.org/issue14894
        if LooseVersion(VERSION.split()[0]) < LooseVersion('1.1.0'):
            is_vlc_available = False
            log.debug('VLC could not be loaded, because the vlc version is too old: %s' % VERSION)
    if is_vlc_available:
        return vlc
    else:
        return None


# On linux we need to initialise X threads, but not when running tests.
# This needs to happen on module load and not in get_vlc(), otherwise it can cause crashes on some DE on some setups
# (reported on Gnome3, Unity, Cinnamon, all GTK+ based) when using native filedialogs...
if is_linux() and 'nose' not in sys.argv[0] and get_vlc():
    try:
        try:
            x11 = ctypes.cdll.LoadLibrary('libX11.so.6')
        except OSError:
            # If libx11.so.6 was not found, fallback to more generic libx11.so
            x11 = ctypes.cdll.LoadLibrary('libX11.so')
        x11.XInitThreads()
    except:
        log.exception('Failed to run XInitThreads(), VLC might not work properly!')


class VlcBackgroundDisplay(MainDisplay):
    """
    Special background display for VLC player
    """
    def __init__(self, parent):
        """
        Constructor
        """
        super(VlcBackgroundDisplay, self).__init__(parent.controller)
        self.parent = parent
        self.is_background = True
        self.has_audio = True
        Registry().remove_function('live_display_hide', self.hide_display)
        Registry().remove_function('live_display_show', self.show_display)
        Registry().remove_function('update_display_css', self.css_changed)

    def event(self, event):
        """
        React to activating of this display
        :param event: The event to be handled
        """
        if event.type() and event.type() == QtCore.QEvent.WindowActivate:
            if self.is_live and self.is_background and self.isActiveWindow():
                self.parent.raise_()
                self.parent.activateWindow()
                return True
        return QtWidgets.QGraphicsView.event(self, event)

    def hide_display(self, mode=HideMode.Screen):
        """
        Hide the display by making all layers transparent Store the images so they can be replaced when required

        :param mode: How the screen is to be hidden
        """
        self.log_debug('hide_display mode = {mode:d}'.format(mode=mode))
        if self.screens.display_count == 1:
            # Only make visible if setting enabled.
            if not Settings().value('core/display on monitor'):
                return
        if mode == HideMode.Screen:
            self.setVisible(False)
        if mode != HideMode.Screen:
            if self.isHidden():
                self.setVisible(True)
        self.hide_mode = mode

    def show_display(self):
        """
        Show the stored layers so the screen reappears as it was originally.
        Make the stored images None to release memory.
        """
        if self.screens.display_count == 1:
            # Only make visible if setting enabled.
            if not Settings().value('core/display on monitor'):
                return
        if self.isHidden():
            self.setVisible(True)
        self.hide_mode = None

    def setup(self):
        """
        Set up and build the output screen
        """
        super(VlcBackgroundDisplay, self).setup()
        self.controller.media_controller.show_player_background(self)
        self.hide_content()
        self.initial_fame = None


class VlcPlayer(MediaPlayer):
    """
    A specialised version of the MediaPlayer class, which provides a VLC display.
    """

    def __init__(self, parent):
        """
        Constructor
        """
        super(VlcPlayer, self).__init__(parent, 'vlc')
        self.original_name = 'VLC'
        self.display_name = '&VLC'
        self.parent = parent
        self.can_background = True
        self.can_folder = True
        self.can_play_list = True
        self.audio_extensions_list = AUDIO_EXT
        self.video_extensions_list = VIDEO_EXT

    def setup(self, display):
        """
        Set up the media player

        :param display: The display where the media is
        :return:
        """
        vlc = get_vlc()
        display.vlc_widget = QtWidgets.QFrame(display)
        display.vlc_widget.setFrameStyle(QtWidgets.QFrame.NoFrame)
        # creating a basic vlc instance
        command_line_options = '--no-video-title-show'
        if not display.has_audio:
            command_line_options += ' --no-audio --no-video-title-show'
        if Settings().value('advanced/hide mouse') and display.controller.is_live:
            command_line_options += ' --mouse-hide-timeout=0'
        if display.is_live and hasattr(display, 'is_background') and display.is_background:
            command_line_options += ' --input-repeat=-1'
        display.vlc_instance = vlc.Instance(command_line_options)
        # creating an empty vlc media player
        display.vlc_media_list_player = display.vlc_instance.media_list_player_new()
        display.vlc_media_player = display.vlc_instance.media_player_new()
        display.vlc_media_list_player.set_media_player(display.vlc_media_player)
        display.vlc_widget.resize(display.size())
        display.vlc_widget.raise_()
        display.vlc_widget.hide()
        # The media player has to be 'connected' to the QFrame.
        # (otherwise a video would be displayed in it's own window)
        # This is platform specific!
        # You have to give the id of the QFrame (or similar object)
        # to vlc, different platforms have different functions for this.
        win_id = int(display.vlc_widget.winId())
        if is_win():
            display.vlc_media_player.set_hwnd(win_id)
        elif is_macosx():
            # We have to use 'set_nsobject' since Qt5 on OSX uses Cocoa
            # framework and not the old Carbon.
            display.vlc_media_player.set_nsobject(win_id)
        else:
            # for Linux/*BSD using the X Server
            display.vlc_media_player.set_xwindow(win_id)
        self.has_own_widget = True
        if display.is_live and not hasattr(display, 'is_background'):
            if hasattr(display, 'vlc_background_display') and display.vlc_background_display:
                display.vlc_background_display.close()
            display.vlc_background_display = VlcBackgroundDisplay(display)
            display.vlc_background_display.setup()
            self.setup(display.vlc_background_display)

    def check_available(self):
        """
        Return the availability of VLC
        """
        return get_vlc() is not None

    def load(self, display):
        """
        Load a video into VLC

        :param display: The display where the media is
        :return:
        """
        display = self._define_display(display)
        vlc = get_vlc()
        log.debug('load vid in Vlc Controller')
        controller = display.controller
        volume = controller.media_list.volume
        if hasattr(display, 'vlc_media_list') and display.vlc_media_list:
            display.vlc_media_list.release()
        display.vlc_media_list = display.vlc_instance.media_list_new()
        display.vlc_media_list.lock()
        display.vlc_media_list_player.set_media_list(display.vlc_media_list)
        # create the media
        if controller.media_list.media_type == MediaType.CD:
            media_info = controller.media_list.get_current()
            file_path = str(media_info.file_info.absoluteFilePath())
            path = os.path.normcase(file_path)
            if is_win():
                path = '/' + path
            display.vlc_media = display.vlc_instance.media_new_location('cdda://' + path)
            display.vlc_media_player.set_media(display.vlc_media)
            display.vlc_media_player.play()
            # Wait for media to start playing. In this case VLC actually returns an error.
            self.media_state_wait(display, vlc.State.Playing)
            # If subitems exists, this is a CD
            audio_cd_tracks = display.vlc_media.subitems()
            if not audio_cd_tracks or audio_cd_tracks.count() < 1:
                return False
            vlc_media = audio_cd_tracks.item_at_index(media_info.title_track)
            display.vlc_media_list.add_media(vlc_media)
        else:
            for media_info in controller.media_list.get():
                file_info = media_info.file_info
                file_path = str(file_info.absoluteFilePath())
                path = os.path.normcase(file_path)
                vlc_media = display.vlc_instance.media_new_path(path)
                display.vlc_media_list.add_media(vlc_media)
        # put the media in the media player
        display.vlc_media = display.vlc_media_list.item_at_index(0)
        display.vlc_media_list.unlock()
        display.vlc_media_list.set_media(display.vlc_media)
        display.vlc_media_player.set_media(display.vlc_media)
        controller.media_list.set_current_index(0)
        # set type of playing: default, loop or repeat
        self.set_playback_loop(display, controller.media_list.can_loop_playback)
        # parse the metadata of the file
        display.vlc_media.parse()
        self.volume(display, volume)
        return True

    def media_state_wait(self, display, media_state):
        """
        Wait for the video to change its state
        Wait no longer than 60 seconds. (loading an iso file needs a long time)

        :param media_state: The state of the playing media
        :param display: The display where the media is
        :return:
        """
        display = self._define_display(display)
        vlc = get_vlc()
        start = datetime.now()
        while media_state != display.vlc_media_list_player.get_state():
            if display.vlc_media_list_player.get_state() == vlc.State.Error:
                return False
            self.application.process_events()
            if (datetime.now() - start).seconds > 60:
                return False
        return True

    def resize(self, display):
        """
        Resize the player

        :param display: The display where the media is
        :return:
        """
        display = self._define_display(display)
        display.vlc_widget.resize(display.size())

    def play(self, display, index=0):
        """
        Play the current item

        :param display: The display where the media is
        :param index: The index of playing item in list
        :return:
        """
        display = self._define_display(display)
        vlc = get_vlc()
        controller = display.controller
        start_time = 0
        log.debug('vlc play')
        if display.controller.is_live:
            if self.get_live_state() != MediaState.Paused and controller.media_list.start_time > 0:
                start_time = controller.media_list.start_time
        else:
            if self.get_preview_state() != MediaState.Paused and controller.media_list.start_time > 0:
                start_time = controller.media_list.start_time
        self.set_playback_loop(display, controller.media_list.can_loop_playback)
        display.vlc_media_list.lock()
        display.vlc_media = display.vlc_media_list.item_at_index(index)
        display.vlc_media_list.unlock()
        is_paused = controller.media_list.current_index() == index and (display.controller.is_live
                                                             and self.get_live_state() == MediaState.Paused
                                                             or not display.controller.is_live
                                                             and self.get_preview_state() == MediaState.Paused)
        if is_paused:
            threading.Thread(target=display.vlc_media_list_player.play).start()
        else:
            threading.Thread(target=display.vlc_media_list_player.play_item_at_index, args=[index]).start()
        controller.media_list.set_current_index(index)
        media_info = controller.media_list.get_current()
        if not self.media_state_wait(display, vlc.State.Playing):
            return False
        if display.controller.is_live:
            if self.get_live_state() != MediaState.Paused and controller.media_list.start_time > 0:
                log.debug('vlc play, start time set')
                start_time = controller.media_list.start_time
        else:
            if self.get_preview_state() != MediaState.Paused and controller.media_list.start_time > 0:
                log.debug('vlc play, start time set')
                start_time = controller.media_list.start_time
        log.debug('mediatype: ' + str(media_info.media_type))
        # Set tracks for the optical device
        if media_info.media_type == MediaType.DVD:
            log.debug('vlc play, playing started')
            if media_info.title_track > 0:
                log.debug('vlc play, title_track set: ' + str(media_info.title_track))
                display.vlc_media_player.set_title(media_info.title_track)
            display.vlc_media_player.play()
            if not self.media_state_wait(display, vlc.State.Playing):
                return False
            if media_info.audio_track > 0:
                display.vlc_media_player.audio_set_track(media_info.audio_track)
                log.debug('vlc play, audio_track set: ' + str(media_info.audio_track))
            if media_info.subtitle_track > 0:
                display.vlc_media_player.video_set_spu(media_info.subtitle_track)
                log.debug('vlc play, subtitle_track set: ' + str(media_info.subtitle_track))
            if controller.media_list.start_time > 0:
                log.debug('vlc play, starttime set: ' + str(controller.media_list.start_time))
                start_time = controller.media_list.start_time
            controller.media_list.length = controller.media_list.end_time - controller.media_list.start_time
            media_info.length = controller.media_list.length
        self.volume(display, controller.media_list.volume)
        if start_time > 0 and display.vlc_media_player.is_seekable():
            display.vlc_media_player.set_time(int(start_time))
        controller.seek_slider.setMaximum(media_info.length)
        self.set_state(MediaState.Playing, display)
        display.vlc_widget.raise_()
        if hasattr(display, 'is_background') and display.is_background:
            controller.display.raise_()
            controller.display.activateWindow()
        return True

    def pause(self, display):
        """
        Pause the current item

        :param display: The display where the media is
        :return:
        """
        display = self._define_display(display)
        vlc = get_vlc()
        if display.vlc_media.get_state() != vlc.State.Playing:
            return
        display.vlc_media_list_player.pause()
        if self.media_state_wait(display, vlc.State.Paused):
            self.set_state(MediaState.Paused, display)

    def stop(self, display):
        """
        Stop the current item

        :param display: The display where the media is
        :return:
        """
        display = self._define_display(display)
        threading.Thread(target=display.vlc_media_list_player.stop).start()
        self.set_state(MediaState.Stopped, display)

    def next(self, display):
        """
        Starts playing next Media File
        """
        display = self._define_display(display)
        threading.Thread(target=display.vlc_media_list_player.next).start()
        self.set_state(MediaState.Playing, display)

    def previous(self, display):
        """
        Starts playing previous Media File
        """
        display = self._define_display(display)
        threading.Thread(target=display.vlc_media_list_player.previous).start()
        self.set_state(MediaState.Playing, display)

    def set_playback_loop(self, display, loop=True):
        """
        Sets playback mode of list of Media Player
        """
        display = self._define_display(display)
        vlc = get_vlc()
        log.debug('set playback mode in Vlc')
        display.vlc_media_list.lock()
        if loop:
            display.vlc_media_list_player.set_playback_mode(vlc.PlaybackMode.loop)
        else:
            display.vlc_media_list_player.set_playback_mode(vlc.PlaybackMode.default)
        display.vlc_media_list.unlock()

    def current_index(self, display):
        """
        Gets index of playing media item of playlist
        """
        display = self._define_display(display)
        log.debug('get index of playing media in Vlc')
        display.vlc_media = display.vlc_media_player.get_media()
        index = display.vlc_media_list.index_of_item(display.vlc_media)
        controller = display.controller
        if controller.media_list.start_time == 0 and controller.media_list.end_time == 0:
            controller.media_list.get()[index].length = display.vlc_media.get_duration()
        return index

    def volume(self, display, vol):
        """
        Set the volume

        :param vol: The volume to be sets
        :param display: The display where the media is
        :return:
        """
        display = self._define_display(display)
        if display.has_audio:
            display.vlc_media_player.audio_set_volume(vol)

    def seek(self, display, seek_value):
        """
        Go to a particular position

        :param seek_value: The position of where a seek goes to
        :param display: The display where the media is
        """
        display = self._define_display(display)
        if display.controller.media_list.media_type == MediaType.CD \
                or display.controller.media_list.media_type == MediaType.DVD:
            seek_value += int(display.controller.media_list.start_time * 1000)
        if display.vlc_media_player.is_seekable():
            display.vlc_media_player.set_time(seek_value)

    def reset(self, display):
        """
        Reset the player

        :param display: The display where the media is
        """
        display = self._define_display(display)
        display.vlc_media_list_player.stop()
        display.vlc_widget.setVisible(False)
        self.set_state(MediaState.Off, display)

    def set_visible(self, display, status):
        """
        Set the visibility

        :param display: The display where the media is
        :param status: The visibility status
        """
        hide_mode = None
        if display.is_live:
            hide_mode = display.hide_mode
        display = self._define_display(display)
        if self.has_own_widget:
            display.vlc_widget.setVisible(status)
        if display.is_live and display.controller.media_list.is_background:
            if status and not hide_mode:
                display.show_display()
            else:
                if not hide_mode:
                    hide_mode = HideMode.Screen
                display.hide_display(hide_mode)

    def update_ui(self, display):
        """
        Update the UI

        :param display: The display where the media is
        """
        display = self._define_display(display)
        vlc = get_vlc()
        # Stop video if playback is finished.
        if display.vlc_media_list_player.get_state() == vlc.State.Ended:
            self.stop(display)
        controller = display.controller
        if controller.media_list.end_time > 0:
            if display.vlc_media_player.get_time() > controller.media_list.end_time * 1000:
                self.stop(display)
                self.set_visible(display, False)
        if not controller.seek_slider.isSliderDown():
            controller.seek_slider.blockSignals(True)
            if display.controller.media_list.media_type == MediaType.CD \
                    or display.controller.media_list.media_type == MediaType.DVD:
                controller.seek_slider.setSliderPosition(display.vlc_media_player.get_time() -
                                                         int(display.controller.media_list.start_time * 1000))
            else:
                display.vlc_media = display.vlc_media_player.get_media()
                controller.media_list.set_current_index(self.current_index(display))
                controller.seek_slider.setMaximum(controller.media_list.get_current().length)
                controller.seek_slider.setSliderPosition(display.vlc_media_player.get_time())
            controller.seek_slider.blockSignals(False)

    def get_info(self):
        """
        Return some information about this player
        """
        return(translate('Media.player', 'VLC is an external player which '
               'supports a number of different formats.') +
               '<br/> <strong>' + translate('Media.player', 'Audio') +
               '</strong><br/>' + str(AUDIO_EXT) + '<br/><strong>' +
               translate('Media.player', 'Video') + '</strong><br/>' +
               str(VIDEO_EXT) + '<br/>')

    @staticmethod
    def _define_display(display):
        """
        Extract the correct display: background display or display itself

        :param display:  Display to define
        """
        if display.controller.media_list.is_background:
            if hasattr(display, 'vlc_background_display'):
                return display.vlc_background_display
            elif hasattr(display, 'is_background') and display.is_background:
                return display
        else:
            if hasattr(display, 'is_background') and display.is_background:
                return display.parent
        return display
