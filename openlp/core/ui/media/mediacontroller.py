# -*- coding: utf-8 -*-
# vim: autoindent shiftwidth=4 expandtab textwidth=120 tabstop=4 softtabstop=4

###############################################################################
# OpenLP - Open Source Lyrics Projection                                      #
# --------------------------------------------------------------------------- #
# Copyright (c) 2008-2017 OpenLP Developers                                   #
# --------------------------------------------------------------------------- #
# This program is free software; you can redistribute it and/or modify it     #
# under the terms of the GNU General Public License as published by the Free  #
# Software Foundation; version 2 of the License.                              #
#                                                                             #
# This program is distributed in the hope that it will be useful, but WITHOUT #
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or       #
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for    #
# more details.                                                               #
#                                                                             #
# You should have received a copy of the GNU General Public License along     #
# with this program; if not, write to the Free Software Foundation, Inc., 59  #
# Temple Place, Suite 330, Boston, MA 02111-1307 USA                          #
###############################################################################
"""
The :mod:`~openlp.core.ui.media.mediacontroller` module contains a base class for media components and other widgets
related to playing media, such as sliders.
"""
import logging
import os
import sys
import datetime
from PyQt5 import QtCore, QtWidgets, QtGui

from openlp.core.common import OpenLPMixin, Registry, RegistryMixin, RegistryProperties, Settings, UiStrings, \
    extension_loader, translate, is_macosx
from openlp.core.lib import ItemCapabilities, ActionAtTheEnd, serviceitem
from openlp.core.lib.ui import critical_error_message_box
from openlp.core.common import AppLocation
from openlp.core.ui import DisplayControllerType
from openlp.core.ui.media.vendor.mediainfoWrapper import MediaInfoWrapper
from openlp.core.ui.media.mediaplayer import MediaPlayer
from openlp.core.ui.media import MediaState, MediaInfo, MediaListInfo, MediaType, get_media_players, set_media_players,\
    parse_optical_path
from openlp.core.ui.lib.toolbar import OpenLPToolbar


log = logging.getLogger(__name__)

TICK_TIME = 200


class MediaSlider(QtWidgets.QSlider):
    """
    Allows the mouse events of a slider to be overridden and extra functionality added
    """
    def __init__(self, direction, manager, controller):
        """
        Constructor
        """
        super(MediaSlider, self).__init__(direction)
        self.manager = manager
        self.controller = controller
        self.no_matching_player = translate('MediaPlugin.MediaItem', 'File %s not supported using player %s')

    def mouseMoveEvent(self, event):
        """
        Override event to allow hover time to be displayed.

        :param event: The triggering event
        """
        time_value = QtWidgets.QStyle.sliderValueFromPosition(self.minimum(), self.maximum(), event.x(), self.width())
        self.setToolTip('%s' % datetime.timedelta(seconds=int(time_value / 1000)))
        QtWidgets.QSlider.mouseMoveEvent(self, event)

    def mousePressEvent(self, event):
        """
        Mouse Press event no new functionality

        :param event: The triggering event
        """
        QtWidgets.QSlider.mousePressEvent(self, event)

    def mouseReleaseEvent(self, event):
        """
        Set the slider position when the mouse is clicked and released on the slider.

        :param event: The triggering event
        """
        self.setValue(QtWidgets.QStyle.sliderValueFromPosition(self.minimum(), self.maximum(), event.x(), self.width()))
        QtWidgets.QSlider.mouseReleaseEvent(self, event)


class MediaController(RegistryMixin, OpenLPMixin, RegistryProperties):
    """
    The implementation of the Media Controller. The Media Controller adds an own class for every Player.
    Currently these are QtWebkit, Phonon and Vlc. display_controllers are an array of controllers keyed on the
    slidecontroller or plugin which built them.

    ControllerType is the class containing the key values.

    media_players are an array of media players keyed on player name.

    current_media_players is an array of player instances keyed on ControllerType.

    """
    def __init__(self, parent=None):
        """
        Constructor
        """
        super(MediaController, self).__init__(parent)
        self.media_players = {}
        self.display_controllers = {}
        self.current_media_players = {}
        # Timer for video state
        self.live_timer = QtCore.QTimer()
        self.live_timer.setInterval(TICK_TIME)
        self.preview_timer = QtCore.QTimer()
        self.preview_timer.setInterval(TICK_TIME)
        # Signals
        self.live_timer.timeout.connect(self.media_state_live)
        self.preview_timer.timeout.connect(self.media_state_preview)
        Registry().register_function('playbackPlay', self.media_play_msg)
        Registry().register_function('playbackPause', self.media_pause_msg)
        Registry().register_function('playbackStop', self.media_stop_msg)
        Registry().register_function('playbackLoop', self.media_loop_msg)
        Registry().register_function('seek_slider', self.media_seek_msg)
        Registry().register_function('volume_slider', self.media_volume_msg)
        Registry().register_function('media_slide', self.media_slide_msg)
        Registry().register_function('media_next', self.media_next_msg)
        Registry().register_function('media_previous', self.media_previous_msg)
        Registry().register_function('media_hide', self.media_hide)
        Registry().register_function('media_blank', self.media_blank)
        Registry().register_function('media_unblank', self.media_unblank)
        # Signals for background video
        Registry().register_function('songs_hide', self.media_hide)
        Registry().register_function('songs_unblank', self.media_unblank)
        Registry().register_function('mediaitem_media_rebuild', self._set_active_players)
        Registry().register_function('mediaitem_suffixes', self._generate_extensions_lists)

    def _set_active_players(self):
        """
        Set the active players and available media files
        """
        saved_players = get_media_players()[0]
        for player in list(self.media_players.keys()):
            self.media_players[player].is_active = player in saved_players

    def _generate_extensions_lists(self):
        """
        Set the active players and available media files
        """
        suffix_list = []
        self.audio_extensions_list = []
        for player in list(self.media_players.values()):
            if player.is_active:
                for item in player.audio_extensions_list:
                    if item not in self.audio_extensions_list:
                        self.audio_extensions_list.append(item)
                        suffix_list.append(item[2:])
        self.video_extensions_list = []
        for player in list(self.media_players.values()):
            if player.is_active:
                for item in player.video_extensions_list:
                    if item not in self.video_extensions_list:
                        self.video_extensions_list.append(item)
                        suffix_list.append(item[2:])
        self.service_manager.supported_suffixes(suffix_list)

    def register_players(self, player):
        """
        Register each media Player (Webkit, Phonon, etc) and store
        for later use

        :param player: Individual player class which has been enabled
        """
        self.media_players[player.name] = player

    def bootstrap_initialise(self):
        """
        Check to see if we have any media Player's available.
        """
        log.debug('_check_available_media_players')
        root = 'openlp'
        if is_macosx() and hasattr(sys, 'frozen') and sys.frozen == 1:
            root = 'MacOS'
        controller_dir = os.path.join(root, 'core', 'ui', 'media')
        glob_pattern = os.path.join(controller_dir, '*player.py')
        extension_loader(glob_pattern, ['mediaplayer.py'])
        player_classes = MediaPlayer.__subclasses__()
        for player_class in player_classes:
            self.register_players(player_class(self))
        if not self.media_players:
            return False
        saved_players, overridden_player = get_media_players()
        invalid_media_players = \
            [media_player for media_player in saved_players if media_player not in self.media_players or
                not self.media_players[media_player].check_available()]
        if invalid_media_players:
            for invalidPlayer in invalid_media_players:
                saved_players.remove(invalidPlayer)
            set_media_players(saved_players, overridden_player)
        self._set_active_players()
        self._generate_extensions_lists()
        return True

    def media_state_live(self):
        """
        Check if there is a running Live media Player and do updating stuff (e.g. update the UI)
        """
        display = self._define_display(self.display_controllers[DisplayControllerType.Live])
        if DisplayControllerType.Live in self.current_media_players:
            self.current_media_players[DisplayControllerType.Live].resize(display)
            self.current_media_players[DisplayControllerType.Live].update_ui(display)
            self.tick(self.display_controllers[DisplayControllerType.Live])
            if self.current_media_players[DisplayControllerType.Live].get_live_state() is not MediaState.Playing:
                self.live_timer.stop()
                if self.current_media_players[DisplayControllerType.Live].get_live_state() is not MediaState.Paused:
                    self.media_stop(self.display_controllers[DisplayControllerType.Live])
        else:
            self.live_timer.stop()
            self.media_stop(self.display_controllers[DisplayControllerType.Live])
        self.update_selected_slide(self.display_controllers[DisplayControllerType.Live])

    def media_state_preview(self):
        """
        Check if there is a running Preview media Player and do updating stuff (e.g. update the UI)
        """
        display = self._define_display(self.display_controllers[DisplayControllerType.Preview])
        if DisplayControllerType.Preview in self.current_media_players:
            self.current_media_players[DisplayControllerType.Preview].resize(display)
            self.current_media_players[DisplayControllerType.Preview].update_ui(display)
            self.tick(self.display_controllers[DisplayControllerType.Preview])
            if self.current_media_players[DisplayControllerType.Preview].get_preview_state() is not MediaState.Playing:
                self.preview_timer.stop()
                if self.current_media_players[
                    DisplayControllerType.Preview].get_preview_state() is not MediaState.Paused:
                    self.media_stop(self.display_controllers[DisplayControllerType.Preview])
        else:
            self.preview_timer.stop()
            self.media_stop(self.display_controllers[DisplayControllerType.Preview])
        self.update_selected_slide(self.display_controllers[DisplayControllerType.Preview])

    @staticmethod
    def update_selected_slide(controller):
        """
        Update selected slide in controller widget if it has changed while playing list
        :param controller: The controller to be played
        """
        if not controller.media_list.is_playing or controller.media_list.is_background:
            return
        current_selected = controller.selected_row
        current_playing = controller.media_list.current_index()
        if controller.media_list.is_playing and current_selected != current_playing and current_playing != -1:
            if controller.is_live:
                prefix = 'live'
            else:
                prefix = 'preview'
            Registry().execute('slidecontroller_{prefix}_change'.format(prefix=prefix), current_playing)

    def get_media_display_css(self):
        """
        Add css style sheets to htmlbuilder
        """
        css = ''
        for player in list(self.media_players.values()):
            if player.is_active:
                css += player.get_media_display_css()
        return css

    def get_media_display_javascript(self):
        """
        Add javascript functions to htmlbuilder
        """
        js = ''
        for player in list(self.media_players.values()):
            if player.is_active:
                js += player.get_media_display_javascript()
        return js

    def get_media_display_html(self):
        """
        Add html code to htmlbuilder
        """
        html = ''
        for player in list(self.media_players.values()):
            if player.is_active:
                html += player.get_media_display_html()
        return html

    def register_controller(self, controller):
        """
        Registers media controls where the players will be placed to run.

        :param controller: The controller where a player will be placed
        """
        self.display_controllers[controller.controller_type] = controller
        self.setup_generic_controls(controller)

    def setup_generic_controls(self, controller):
        """
        Set up controls on the control_panel for a given controller

        :param controller:  First element is the controller which should be used
        """
        controller.media_list = MediaListInfo()
        # Build a Media ToolBar
        controller.mediabar = OpenLPToolbar(controller)
        controller.mediabar.add_toolbar_action('playbackPlay', text='media_playback_play',
                                               icon=':/slides/media_playback_start.png',
                                               tooltip=translate('OpenLP.SlideController', 'Start playing media.'),
                                               triggers=controller.send_to_plugins)
        controller.mediabar.add_toolbar_action('playbackPause', text='media_playback_pause',
                                               icon=':/slides/media_playback_pause.png',
                                               tooltip=translate('OpenLP.SlideController', 'Pause playing media.'),
                                               triggers=controller.send_to_plugins)
        controller.mediabar.add_toolbar_action('playbackStop', text='media_playback_stop',
                                               icon=':/slides/media_playback_stop.png',
                                               tooltip=translate('OpenLP.SlideController', 'Stop playing media.'),
                                               triggers=controller.send_to_plugins)
        controller.mediabar.add_toolbar_action('playbackLoop', text='media_playback_loop',
                                               icon=':/media/media_repeat.png', checked=False,
                                               tooltip=translate('OpenLP.SlideController', 'Loop playing media.'),
                                               triggers=controller.send_to_plugins)
        controller.position_label = QtWidgets.QLabel()
        controller.position_label.setText(' 00:00 / 00:00')
        controller.position_label.setToolTip(translate('OpenLP.SlideController', 'Video timer.'))
        controller.position_label.setObjectName('position_label')
        controller.mediabar.add_toolbar_widget(controller.position_label)
        # Build the seek_slider.
        controller.seek_slider = MediaSlider(QtCore.Qt.Horizontal, self, controller)
        controller.seek_slider.setMaximum(1000)
        controller.seek_slider.setTracking(True)
        controller.seek_slider.setMouseTracking(True)
        controller.seek_slider.setToolTip(translate('OpenLP.SlideController', 'Video position.'))
        controller.seek_slider.setGeometry(QtCore.QRect(90, 260, 221, 24))
        controller.seek_slider.setObjectName('seek_slider')
        controller.mediabar.add_toolbar_widget(controller.seek_slider)
        # Build the volume_slider.
        controller.volume_slider = QtWidgets.QSlider(QtCore.Qt.Horizontal)
        controller.volume_slider.setTickInterval(10)
        controller.volume_slider.setTickPosition(QtWidgets.QSlider.TicksAbove)
        controller.volume_slider.setMinimum(0)
        controller.volume_slider.setMaximum(100)
        controller.volume_slider.setTracking(True)
        controller.volume_slider.setToolTip(translate('OpenLP.SlideController', 'Audio Volume.'))
        controller.volume_slider.setValue(controller.media_list.volume)
        controller.volume_slider.setGeometry(QtCore.QRect(90, 160, 221, 24))
        controller.volume_slider.setObjectName('volume_slider')
        controller.mediabar.add_toolbar_widget(controller.volume_slider)
        controller.controller_layout.addWidget(controller.mediabar)
        controller.mediabar.setVisible(False)
        if not controller.is_live:
            controller.volume_slider.setEnabled(False)
        # Signals
        controller.seek_slider.valueChanged.connect(controller.send_to_plugins)
        controller.volume_slider.valueChanged.connect(controller.send_to_plugins)

    def setup_display(self, display, preview):
        """
        After a new display is configured, all media related widgets will be created too

        :param display:  Display on which the output is to be played
        :param preview: Whether the display is a main or preview display
        """
        # clean up possible running old media files
        self.finalise()
        # update player status
        self._set_active_players()
        display.has_audio = True
        if display.is_live and preview:
            return
        if preview:
            display.has_audio = False
        for player in list(self.media_players.values()):
            if player.is_active:
                player.setup(display)

    def set_controls_visible(self, controller, value):
        """
        After a new display is configured, all media related widget will be created too

        :param controller: The controller on which controls act.
        :param value: control name to be changed.
        """
        # Generic controls
        controller.mediabar.setVisible(value)
        if controller.is_live and controller.display:
            if self.current_media_players and value:
                if not self.current_media_players[controller.controller_type].can_background:
                    controller.display.set_transparency(False)

    @staticmethod
    def resize(display, player):
        """
        After Mainwindow changes or Splitter moved all related media widgets have to be resized

        :param display: The display on which output is playing.
        :param player:  The player which is doing the playing.
        """
        player.resize(display)

    def video(self, source, service_item, index=0, hidden=False, video_behind_text=False):
        """
        Loads and starts a video to run with the option of sound

        :param source: Where the call originated form
        :param service_item: The player which is doing the playing
        :param index: index of playing file in the list, 0 has to be passed by default
        :param hidden: The player which is doing the playing
        :param video_behind_text: Is the video to be played behind text.
        """
        is_valid = False
        controller = self.display_controllers[source]
        # stop running videos
        self.media_reset(controller)
        controller.media_list = MediaListInfo()
        controller.media_list.clear()
        controller.media_list.volume = service_item.volume
        controller.media_list.is_background = video_behind_text
        # background will always loop video.
        controller.media_list.can_loop_playback = service_item.action_at_the_end == ActionAtTheEnd.Loop \
                                                  or video_behind_text
        is_multi_files = len(service_item.get_frames()) > 1
        for row in range(len(service_item.get_frames())):
            media_info = MediaInfo()
            file_path = service_item.get_frame_path(row)
            media_info.file_info = QtCore.QFileInfo(file_path)
            if is_multi_files:
                media_data = MediaInfoWrapper.parse(file_path)
                media_info.length = media_data.tracks[0].duration
            controller.media_list.append(media_info)
        controller.media_list.set_current_index(index)
        display = self._define_display(controller)
        if controller.is_live:
            # if this is an optical device use special handling
            if service_item.is_capable(ItemCapabilities.IsOptical):
                log.debug('video is optical and live')
                path = service_item.get_frame_path()
                (name, title, audio_track, subtitle_track, start, end, clip_name) = parse_optical_path(path)
                is_valid = self.media_setup_optical(name, title, audio_track, subtitle_track, start, end, display,
                                                    controller)
            else:
                log.debug('video is not optical and live')
                controller.media_list.length = service_item.media_length
                is_valid = self._check_file_type(controller, display, service_item)
            display.override['theme'] = ''
            display.override['video'] = True
            if controller.media_list.is_background:
                # ignore start/end time
                controller.media_list.start_time = 0
                controller.media_list.end_time = 0
            else:
                controller.media_list.start_time = service_item.start_time
                controller.media_list.end_time = service_item.end_time
        elif controller.preview_display:
            if service_item.is_capable(ItemCapabilities.IsOptical):
                log.debug('video is optical and preview')
                path = service_item.get_frame_path()
                (name, title, audio_track, subtitle_track, start, end, clip_name) = parse_optical_path(path)
                is_valid = self.media_setup_optical(name, title, audio_track, subtitle_track, start, end, display,
                                                    controller)
            else:
                log.debug('video is not optical and preview')
                controller.media_list.length = service_item.media_length
                is_valid = self._check_file_type(controller, display, service_item)
        if not is_valid:
            # Media could not be loaded correctly
            critical_error_message_box(translate('MediaPlugin.MediaItem', 'Unsupported File'),
                                       translate('MediaPlugin.MediaItem', 'Unsupported File'))
            return False
        if not is_multi_files:
            controller.media_list.get_current().length = controller.media_list.length
        controller.mediabar.actions['playbackLoop'].setChecked(controller.media_list.can_loop_playback)
        log.debug('video mediatype: ' + str(controller.media_list.media_type))
        # dont care about actual theme, set a black background
        if controller.is_live and not controller.media_list.is_background:
            if not self.current_media_players[controller.controller_type].has_own_widget:
                self.show_player_background(display)
            display.hide_content()
        # now start playing - Preview is autoplay!
        autoplay = False
        # Preview requested
        if not controller.is_live:
            autoplay = True
        # Visible or background requested or Service Item wants to autostart
        elif not hidden or controller.media_list.is_background or service_item.will_auto_start:
            autoplay = True
        # Unblank on load set
        elif Settings().value('core/auto unblank'):
            autoplay = True
        if autoplay:
            if not self.media_play(controller, index):
                critical_error_message_box(translate('MediaPlugin.MediaItem', 'Unsupported File'),
                                           translate('MediaPlugin.MediaItem', 'Unsupported File'))
                return False
        self.set_controls_visible(controller, True)
        log.debug('use %s controller' % self.current_media_players[controller.controller_type].display_name)
        return True

    @staticmethod
    def show_player_background(display):
        """
        Show black layer on top of all other layers
        """
        background_color = QtGui.QColor()
        background_color.setNamedColor(Settings().value('players/background color'))
        if not background_color.isValid():
            background_color = QtCore.Qt.black
        color_name = background_color.name()
        bg_html = '<div class="frame" id="black" style="background-color: {color};"></div>'.format(color=color_name)
        display.show_next_frame(bg_html, True)

    def media_length(self, service_item):
        """
        Loads and starts a media item to obtain the media length

        :param service_item: The ServiceItem containing the details to be played.
        """
        unsupported_files = []
        duration = 0
        used_players = get_media_players()[0]
        player = self.media_players[used_players[0]]
        for row in range(len(service_item.get_frames())):
            media_info = MediaInfo()
            file_path = service_item.get_frame_path(row)
            media_info.file_info = QtCore.QFileInfo(file_path)
            # display = controller.preview_display
            suffix = '*.%s' % media_info.file_info.suffix().lower()
            if suffix not in player.video_extensions_list and suffix not in player.audio_extensions_list:
                unsupported_files.append(file_path)
                continue
            media_data = MediaInfoWrapper.parse(file_path)
            # duration returns in milli seconds
            duration += media_data.tracks[0].duration
        if len(unsupported_files) > 0:
            # Media could not be loaded correctly
            critical_error_message_box(
                translate('MediaPlugin.MediaItem', 'Unsupported Media File'),
                translate('MediaPlugin.MediaItem', 'File {file_path} not supported using player {player_name}'
                          ).format(file_path='\n '.join(unsupported_files), player_name=used_players[0]))
        if len(unsupported_files) == len(service_item.get_frames()):
            return False
        service_item.set_media_length(duration)
        return True

    def media_setup_optical(self, filename, title, audio_track, subtitle_track, start, end, display, controller):
        """
        Setup playback of optical media

        :param filename: Path of the optical device/drive.
        :param title: The main/title track to play.
        :param audio_track: The audio track to play.
        :param subtitle_track: The subtitle track to play.
        :param start: Start position in milliseconds.
        :param end: End position in milliseconds.
        :param display: The display to play the media.
        :param controller: The media controller.
        :return: True if setup succeeded else False.
        """
        if controller is None:
            controller = self.display_controllers[DisplayControllerType.Plugin]
        # stop running videos
        self.media_reset(controller)
        # Setup media info
        # TODO: Fix this
        controller.media_list.clear()
        media_info = MediaInfo()
        media_info.file_info = QtCore.QFileInfo(filename)
        if audio_track == -1 and subtitle_track == -1:
            media_info.media_type = MediaType.CD
        else:
            media_info.media_type = MediaType.DVD
        controller.media_list.start_time = start // 1000
        controller.media_list.end_time = end // 1000
        controller.media_list.length = (end - start) // 1000
        media_info.title_track = title
        media_info.audio_track = audio_track
        media_info.subtitle_track = subtitle_track
        # When called from mediaitem display is None
        if display is None:
            display = controller.preview_display
        # Find vlc player
        used_players = get_media_players()[0]
        vlc_player = None
        for title in used_players:
            player = self.media_players[title]
            if player.name == 'vlc':
                vlc_player = player
        if vlc_player is None:
            critical_error_message_box(translate('MediaPlugin.MediaItem', 'VLC player required'),
                                       translate('MediaPlugin.MediaItem',
                                                 'VLC player required for playback of optical devices'))
            return False
        vlc_player.load(display)
        self.resize(display, vlc_player)
        self.current_media_players[controller.controller_type] = vlc_player
        if audio_track == -1 and subtitle_track == -1:
            media_info.media_type = MediaType.CD
        else:
            media_info.media_type = MediaType.DVD
        controller.media_list.append(media_info)
        return True

    @staticmethod
    def _get_used_players(service_item):
        """
        Find the player for a given service item

        :param service_item: where the information is about the media and required player
        :return: player description
        """
        used_players = get_media_players()[0]
        # If no player, we can't play
        if not used_players:
            return False
        default_player = [used_players[0]]
        if service_item.processor and service_item.processor != UiStrings().Automatic:
            # check to see if the player is usable else use the default one.
            if service_item.processor.lower() not in used_players:
                used_players = default_player
            else:
                used_players = [service_item.processor.lower()]
        return used_players

    def _check_file_type(self, controller, display, service_item):
        """
        Select the correct media Player type from the prioritized Player list

        :param controller: First element is the controller which should be used
        :param display: Which display to use
        :param service_item: The ServiceItem containing the details to be played.
        """
        used_players = self._get_used_players(service_item)
        is_list = len(controller.media_list.get()) > 1
        media_info = controller.media_list.get_current()
        if media_info.file_info.isFile():
            suffix = '*.%s' % media_info.file_info.suffix().lower()
            for title in used_players:
                if not title:
                    continue
                player = self.media_players[title]
                if is_list and not player.can_play_list:
                    continue
                # The system player may not return what files it can play so add it now
                #  and check whether it can play the file later
                if title == 'system':
                    if not controller.media_list.is_background or controller.media_list.is_background and \
                            player.can_background:
                        self.resize(display, player)
                        if player.load(display):
                            self.current_media_players[controller.controller_type] = player
                            media_info.media_type = MediaType.Video
                            controller.media_list.media_type = MediaType.Video
                            return True
                if suffix in player.video_extensions_list:
                    if not controller.media_list.is_background or controller.media_list.is_background and \
                            player.can_background:
                        self.resize(display, player)
                        if player.load(display):
                            self.current_media_players[controller.controller_type] = player
                            media_info.media_type = MediaType.Video
                            controller.media_list.media_type = MediaType.Video
                            return True
                if suffix in player.audio_extensions_list:
                    if player.load(display):
                        self.current_media_players[controller.controller_type] = player
                        media_info.media_type = MediaType.Audio
                        controller.media_list.media_type = MediaType.Audio
                        return True
        else:
            for title in used_players:
                player = self.media_players[title]
                if player.can_folder:
                    self.resize(display, player)
                    if player.load(display):
                        self.current_media_players[controller.controller_type] = player
                        media_info.media_type = MediaType.Video
                        controller.media_list.media_type = MediaType.Video
                        return True
        # no valid player found
        return False

    def media_play_msg(self, msg, status=True):
        """
        Responds to the request to play a loaded video

        :param msg: First element is the controller which should be used
        :param status:
        """
        controller = msg[0]
        index = controller.media_list.current_index()
        self.media_play(controller, index, status)

    def media_play(self, controller, index, first_time=True):
        """
        Responds to the request to play a loaded video

        :param index: index of playing item in the list
        :param controller: The controller to be played
        :param first_time:
        """
        controller.seek_slider.blockSignals(True)
        controller.volume_slider.blockSignals(True)
        display = self._define_display(controller)
        if not self.current_media_players[controller.controller_type].play(display, index):
            controller.seek_slider.blockSignals(False)
            controller.volume_slider.blockSignals(False)
            return False
        self.media_volume(controller, controller.media_list.volume)
        if first_time:
            if display.is_live and self.current_media_players[controller.controller_type].has_own_widget:
                display.hide_background()
            self.current_media_players[controller.controller_type].set_visible(display, True)
            controller.mediabar.actions['playbackPlay'].setVisible(False)
            controller.mediabar.actions['playbackPause'].setVisible(True)
            controller.mediabar.actions['playbackStop'].setDisabled(False)
        if controller.is_live:
            if controller.hide_menu.defaultAction().isChecked() and not controller.media_list.is_background:
                controller.hide_menu.defaultAction().trigger()
            # Start Timer for ui updates
            if not self.live_timer.isActive():
                self.live_timer.start()
        else:
            # Start Timer for ui updates
            if not self.preview_timer.isActive():
                self.preview_timer.start()
        controller.seek_slider.blockSignals(False)
        controller.volume_slider.blockSignals(False)
        controller.media_list.is_playing = True
        display = self._define_display(controller)
        display.setVisible(True)
        if controller.media_list.is_playing and not controller.media_list.is_background:
            if hasattr(display, 'service_item') and len(display.service_item.get_frames()) > index:
                frame = display.service_item.get_frames()[index]
                display.current_raw_frame = os.path.join(frame['path'], frame['title'])
        return True

    def tick(self, controller):
        """
        Add a tick while the media is playing but only count if not paused

        :param controller:  The Controller to be processed
        """
        media_info = controller.media_list.get_current()
        if controller.media_list.is_playing and media_info.length > 0:
            controller.media_list.timer += TICK_TIME
            seconds = controller.seek_slider.sliderPosition() // 1000
            minutes = seconds // 60
            seconds %= 60
            total_seconds = media_info.length // 1000
            total_minutes = total_seconds // 60
            total_seconds %= 60
            controller.position_label.setText(' %02d:%02d / %02d:%02d' %
                                              (minutes, seconds, total_minutes, total_seconds))

    def media_pause_msg(self, msg):
        """
        Responds to the request to pause a loaded video

        :param msg: First element is the controller which should be used
        """
        self.media_pause(msg[0])

    def media_pause(self, controller):
        """
        Responds to the request to pause a loaded video

        :param controller: The Controller to be paused
        """
        display = self._define_display(controller)
        if controller.controller_type in self.current_media_players:
            self.current_media_players[controller.controller_type].pause(display)
            controller.mediabar.actions['playbackPlay'].setVisible(True)
            controller.mediabar.actions['playbackStop'].setDisabled(False)
            controller.mediabar.actions['playbackPause'].setVisible(False)
            controller.media_list.is_playing = False
            if controller.is_live:
                if self.live_timer.isActive():
                    self.live_timer.stop()
            else:
                if self.preview_timer.isActive():
                    self.preview_timer.stop()

    def media_loop_msg(self, msg):
        """
        Responds to the request to loop a loaded video

        :param msg: First element is the controller which should be used
        """
        self.media_loop(msg[0])

    def media_loop(self, controller):
        """
        Responds to the request to loop a loaded video

        :param controller: The controller that needs to be stopped
        """
        controller.media_list.can_loop_playback = not controller.media_list.can_loop_playback
        controller.mediabar.actions['playbackLoop'].setChecked(controller.media_list.can_loop_playback)
        display = self._define_display(controller)
        player = self.current_media_players[controller.controller_type]
        player.set_playback_loop(display, controller.media_list.can_loop_playback)

    def media_stop_msg(self, msg):
        """
        Responds to the request to stop a loaded video

        :param msg: First element is the controller which should be used
        """
        self.media_stop(msg[0])

    def media_stop(self, controller, looping_background=False):
        """
        Responds to the request to stop a loaded video

        :param controller: The controller that needs to be stopped
        :param looping_background: The background is looping so do not blank.
        """
        display = self._define_display(controller)
        if controller.controller_type in self.current_media_players:
            if not looping_background:
                if display.is_live and not display.hide_mode:
                    display.show_blackscreen()
                self.current_media_players[controller.controller_type].stop(display)
                self.current_media_players[controller.controller_type].set_visible(display, False)
                controller.media_list.is_playing = False
                controller.mediabar.actions['playbackPlay'].setVisible(True)
                controller.mediabar.actions['playbackStop'].setDisabled(True)
                controller.mediabar.actions['playbackPause'].setVisible(False)
            controller.seek_slider.blockSignals(True)
            controller.seek_slider.setSliderPosition(0)
            controller.seek_slider.blockSignals(False)
            controller.media_list.timer = 1000
            controller.media_timer = 0

    def media_slide_msg(self, msg):
        """
        Blank the related video Widget

        :param msg: First element is the boolean for Live indication
            Second element is the hide mode
        """
        service_item = msg[0]
        is_live = msg[1]
        index = msg[2]
        if len(service_item.get_frames()) == 1:
            return
        if is_live:
            controller = self.live_controller
        else:
            controller = self.preview_controller
        if controller.media_list.current_index() == index:
            return
        self.media_play(controller, index)

    def media_next_msg(self, msg):
        """
        Blank the related video Widget

        :param msg: First element is the boolean for Live indication
            Second element is the hide mode
        """
        service_item = msg[0]
        is_live = msg[1]
        self.media_next(service_item, is_live)

    def media_next(self, service_item, is_live):
        """
        Blank the related video Widget

        :param msg: First element is the boolean for Live indication
            Second element is the hide mode
        """
        # Should not react on slide select if only one slide
        if len(service_item.get_frames()) == 1:
            return
        if is_live:
            controller = self.live_controller
        else:
            controller = self.preview_controller
        controller.seek_slider.blockSignals(True)
        controller.volume_slider.blockSignals(True)
        display = self._define_display(controller)
        if not self.current_media_players[controller.controller_type].next(display):
            controller.seek_slider.blockSignals(False)
            controller.volume_slider.blockSignals(False)

    def media_previous_msg(self, msg):
        """
        Blank the related video Widget

        :param msg: First element is the boolean for Live indication
            Second element is the hide mode
        """
        service_item = msg[0]
        is_live = msg[1]
        self.media_previous(service_item, is_live)

    def media_previous(self, service_item, is_live):
        """
        Blank the related video Widget

        :param msg: First element is the boolean for Live indication
            Second element is the hide mode
        """
        # Should not react on slide select if only one slide
        if len(service_item.get_frames()) == 1:
            return
        if is_live:
            controller = self.live_controller
        else:
            controller = self.preview_controller
        controller.seek_slider.blockSignals(True)
        controller.volume_slider.blockSignals(True)
        display = self._define_display(controller)
        if not self.current_media_players[controller.controller_type].previous(display):
            controller.seek_slider.blockSignals(False)
            controller.volume_slider.blockSignals(False)

    def update_action_at_the_end(self, action_at_the_end):
        """
        Updates action at the end on live controller

        :param action_at_the_end: Action at the end of Service item
        """
        controller = self.live_controller
        display = self._define_display(controller)
        controller.media_list.can_loop_playback = action_at_the_end == ActionAtTheEnd.Loop
        player = self.current_media_players[controller.controller_type]
        player.set_playback_loop(display, controller.media_list.can_loop_playback)

    def media_volume_msg(self, msg):
        """
        Changes the volume of a running video

        :param msg: First element is the controller which should be used
        """
        controller = msg[0]
        vol = msg[1][0]
        self.media_volume(controller, vol)

    def media_volume(self, controller, volume):
        """
        Changes the volume of a running video

        :param controller: The Controller to use
        :param volume: The volume to be set
        """
        log.debug('media_volume %d' % volume)
        display = self._define_display(controller)
        self.current_media_players[controller.controller_type].volume(display, volume)
        controller.volume_slider.setValue(volume)
        controller.media_list.volume = volume

    def media_seek_msg(self, msg):
        """
        Responds to the request to change the seek Slider of a loaded video via a message

        :param msg: First element is the controller which should be used
            Second element is a list with the seek value as first element
        """
        controller = msg[0]
        seek_value = msg[1][0]
        self.media_seek(controller, seek_value)

    def media_seek(self, controller, seek_value):
        """
        Responds to the request to change the seek Slider of a loaded video

        :param controller: The controller to use.
        :param seek_value: The value to set.
        """
        display = self._define_display(controller)
        self.current_media_players[controller.controller_type].seek(display, seek_value)
        controller.media_list.timer = seek_value

    def media_reset(self, controller):
        """
        Responds to the request to reset a loaded video
        :param controller: The controller to use.
        """
        self.set_controls_visible(controller, False)
        display = self._define_display(controller)
        if controller.controller_type in self.current_media_players:
            display.override = {}
            if controller.is_live:
                if self.live_timer.isActive():
                    self.live_timer.stop()
            else:
                if self.preview_timer.isActive():
                    self.preview_timer.stop()
            if self.current_media_players[controller.controller_type].has_own_widget:
                if hasattr(display, 'service_item') and (
                    not display.service_item.is_media() or controller.media_list.is_background):
                    display.show_background(display.service_item)
                else:
                    self.show_player_background(display)
            if display.is_live:
                display.wait_show_complete()
            self.current_media_players[controller.controller_type].reset(display)
            self.current_media_players[controller.controller_type].set_visible(display, False)
            del self.current_media_players[controller.controller_type]

    def media_hide(self, msg):
        """
        Hide the related video Widget

        :param msg: First element is the boolean for Live indication
        """
        is_live = msg[1]
        if not is_live:
            return
        display = self._define_display(self.live_controller)
        if self.live_controller.controller_type in self.current_media_players and \
                self.current_media_players[self.live_controller.controller_type].get_live_state() == MediaState.Playing:
            self.current_media_players[self.live_controller.controller_type].pause(display)
            self.current_media_players[self.live_controller.controller_type].set_visible(display, False)

    def media_blank(self, msg):
        """
        Blank the related video Widget

        :param msg: First element is the boolean for Live indication
            Second element is the hide mode
        """
        is_live = msg[1]
        hide_mode = msg[2]
        if not is_live:
            return
        Registry().execute('live_display_hide', hide_mode)
        display = self._define_display(self.live_controller)
        if self.live_controller.controller_type in self.current_media_players and \
                self.current_media_players[self.live_controller.controller_type].get_live_state() == MediaState.Playing:
            self.current_media_players[self.live_controller.controller_type].pause(display)
            self.current_media_players[self.live_controller.controller_type].set_visible(display, False)

    def media_unblank(self, msg):
        """
        Unblank the related video Widget

        :param msg: First element is not relevant in this context
            Second element is the boolean for Live indication
        """
        Registry().execute('live_display_show')
        is_live = msg[1]
        if not is_live:
            return
        display = self._define_display(self.live_controller)
        if self.live_controller.controller_type in self.current_media_players and \
                self.current_media_players[self.live_controller.controller_type].get_live_state() != \
                MediaState.Playing:
            index = self.live_controller.media_list.current_index()
            if self.current_media_players[self.live_controller.controller_type].play(display, index):
                self.current_media_players[self.live_controller.controller_type].set_visible(display, True)
                # Start Timer for ui updates
                if not self.live_timer.isActive():
                    self.live_timer.start()

    def finalise(self):
        """
        Reset all the media controllers when OpenLP shuts down
        """
        self.live_timer.stop()
        self.preview_timer.stop()
        for controller in self.display_controllers:
            self.media_reset(self.display_controllers[controller])

    @staticmethod
    def _define_display(controller):
        """
        Extract the correct display for a given controller

        :param controller:  Controller to be used
        """
        if controller.is_live:
            return controller.display
        return controller.preview_display
