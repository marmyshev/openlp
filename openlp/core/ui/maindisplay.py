# -*- coding: utf-8 -*-
# vim: autoindent shiftwidth=4 expandtab textwidth=120 tabstop=4 softtabstop=4

###############################################################################
# OpenLP - Open Source Lyrics Projection                                      #
# --------------------------------------------------------------------------- #
# Copyright (c) 2008-2017 OpenLP Developers                                   #
# --------------------------------------------------------------------------- #
# This program is free software; you can redistribute it and/or modify it     #
# under the terms of the GNU General Public License as published by the Free  #
# Software Foundation; version 2 of the License.                              #
#                                                                             #
# This program is distributed in the hope that it will be useful, but WITHOUT #
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or       #
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for    #
# more details.                                                               #
#                                                                             #
# You should have received a copy of the GNU General Public License along     #
# with this program; if not, write to the Free Software Foundation, Inc., 59  #
# Temple Place, Suite 330, Boston, MA 02111-1307 USA                          #
###############################################################################
"""
The :mod:`maindisplay` module provides the functionality to display screens and play multimedia within OpenLP.

Some of the code for this form is based on the examples at:

* `http://www.steveheffernan.com/html5-video-player/demo-video-player.html`_
* `http://html5demos.com/two-videos`_

"""

import html
import logging
import os
from datetime import datetime

from PyQt5 import QtCore, QtWidgets, QtWebKit, QtWebKitWidgets, QtGui, QtMultimedia

from openlp.core.common import AppLocation, Registry, RegistryProperties, OpenLPMixin, Settings, translate,\
    is_macosx, is_win, UiStrings
from openlp.core.lib import ServiceItem, ImageSource, ScreenList, build_html_display, build_html_text,\
    build_html_image, build_html_background, build_html_footer, expand_tags, image_to_byte
from openlp.core.lib.theme import BackgroundType
from openlp.core.ui import HideMode, AlertLocation, DisplayControllerType

if is_macosx():
    from ctypes import pythonapi, c_void_p, c_char_p, py_object

    from sip import voidptr
    from objc import objc_object
    from AppKit import NSMainMenuWindowLevel, NSWindowCollectionBehaviorManaged

log = logging.getLogger(__name__)

OPAQUE_STYLESHEET = """
QWidget {
    border: 0px;
    margin: 0px;
    padding: 0px;
}
QGraphicsView {}
"""
TRANSPARENT_STYLESHEET = """
QWidget {
    border: 0px;
    margin: 0px;
    padding: 0px;
}
QGraphicsView {
    background: transparent;
    border: 0px;
}
"""


class Display(QtWidgets.QGraphicsView):
    """
    This is a general display screen class. Here the general display settings will done. It will be used as
    specialized classes by Main Display and Preview display.
    """
    def __init__(self, parent):
        """
        Constructor
        """
        self.is_live = False
        if hasattr(parent, 'is_live') and parent.is_live:
            self.is_live = True
        if self.is_live:
            self.parent = lambda: parent
        super(Display, self).__init__()
        self.controller = parent
        self.screen = {}
        self.scene = QtWidgets.QGraphicsScene()
        self.setScene(self.scene)

    def setup(self):
        """
        Set up and build the screen base
        """
        self.setGeometry(self.screen['size'])
        self.web_view = QtWebKitWidgets.QWebView(self)
        self.web_view.setGeometry(0, 0, self.screen['size'].width(), self.screen['size'].height())
        self.web_view.settings().setAttribute(QtWebKit.QWebSettings.PluginsEnabled, True)
        palette = self.web_view.palette()
        palette.setBrush(QtGui.QPalette.Base, QtCore.Qt.transparent)
        self.web_view.page().setPalette(palette)
        self.web_view.setAttribute(QtCore.Qt.WA_OpaquePaintEvent, False)
        self.page = self.web_view.page()
        self.frame = self.page.mainFrame()
        if self.is_live and log.getEffectiveLevel() == logging.DEBUG:
            self.web_view.settings().setAttribute(QtWebKit.QWebSettings.DeveloperExtrasEnabled, True)
        self.web_view.loadFinished.connect(self.is_web_loaded)
        self.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.frame.setScrollBarPolicy(QtCore.Qt.Vertical, QtCore.Qt.ScrollBarAlwaysOff)
        self.frame.setScrollBarPolicy(QtCore.Qt.Horizontal, QtCore.Qt.ScrollBarAlwaysOff)
        self.web_view_proxy = self.scene.addWidget(self.web_view)

    def resizeEvent(self, event):
        """
        React to resizing of this display

        :param event: The event to be handled
        """
        if hasattr(self, 'web_view'):
            self.web_view.setGeometry(0, 0, self.width(), self.height())

    def is_web_loaded(self, field=None):
        """
        Called by webView event to show display is fully loaded
        """
        self.web_loaded = True

    def show_next_frame(self, frame_html, is_background=False):
        """
        Show next frame on screen

        :param frame_html: HTML content to show in the next frame
        :param is_background: is to show it in background frames or in content frames
        """
        if is_background:
            next_frame_id = self.frame.evaluateJavaScript('get_next_frame_id(true);')
            if not next_frame_id:
                next_frame_id = 'bg_frame1'
        else:
            next_frame_id = self.frame.evaluateJavaScript('get_next_frame_id();')
            if not next_frame_id:
                next_frame_id = 'frame1'
        self.frame.findFirstElement('#' + next_frame_id).setInnerXml(frame_html)
        if self.is_live and self.hide_mode and not (is_background and self.hide_mode == HideMode.Theme):
            return
        if is_background:
            self.frame.evaluateJavaScript('show_next(true);')
        else:
            self.frame.evaluateJavaScript('show_next();')


class MainDisplay(OpenLPMixin, Display, RegistryProperties):
    """
    This is the display screen as a specialized class from the Display class
    """
    def __init__(self, parent):
        """
        Constructor
        """
        super(MainDisplay, self).__init__(parent)
        self.screens = ScreenList()
        self.rebuild_css = False
        self.hide_mode = None
        self.current_raw_frame = None
        self.current_background = None
        self.current_background_type = None
        self.override = {}
        self.retranslateUi()
        self.media_object = None
        if self.is_live:
            self.audio_player = AudioPlayer(self)
        else:
            self.audio_player = None
        self.first_time = True
        self.web_loaded = True
        self.setStyleSheet(OPAQUE_STYLESHEET)
        window_flags = QtCore.Qt.FramelessWindowHint | QtCore.Qt.Tool | QtCore.Qt.WindowStaysOnTopHint
        if Settings().value('advanced/x11 bypass wm'):
            window_flags |= QtCore.Qt.X11BypassWindowManagerHint
        # TODO: The following combination of window_flags works correctly
        # on Mac OS X. For next OpenLP version we should test it on other
        # platforms. For OpenLP 2.0 keep it only for OS X to not cause any
        # regressions on other platforms.
        if is_macosx():
            window_flags = QtCore.Qt.FramelessWindowHint | QtCore.Qt.Window | QtCore.Qt.NoDropShadowWindowHint
        self.setWindowFlags(window_flags)
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        self.set_transparency(self.is_live)
        if is_macosx():
            if self.is_live:
                # Get a pointer to the underlying NSView
                try:
                    nsview_pointer = self.winId().ascapsule()
                except:
                    nsview_pointer = voidptr(self.winId()).ascapsule()
                # Set PyCapsule name so pyobjc will accept it
                pythonapi.PyCapsule_SetName.restype = c_void_p
                pythonapi.PyCapsule_SetName.argtypes = [py_object, c_char_p]
                pythonapi.PyCapsule_SetName(nsview_pointer, c_char_p(b"objc.__object__"))
                # Covert the NSView pointer into a pyobjc NSView object
                self.pyobjc_nsview = objc_object(cobject=nsview_pointer)
                # Set the window level so that the MainDisplay is above the menu bar and dock
                self.pyobjc_nsview.window().setLevel_(NSMainMenuWindowLevel + 2)
                # Set the collection behavior so the window is visible when Mission Control is activated
                self.pyobjc_nsview.window().setCollectionBehavior_(NSWindowCollectionBehaviorManaged)
                if self.screens.current['primary']:
                    # Connect focusWindowChanged signal so we can change the window level when the display is not in
                    # focus on the primary screen
                    self.application.focusWindowChanged.connect(self.change_window_level)
        if self.is_live:
            Registry().register_function('live_display_hide', self.hide_display)
            Registry().register_function('live_display_show', self.show_display)
            Registry().register_function('update_display_css', self.css_changed)
        self.close_display = False

    def closeEvent(self, event):
        """
        Catch the close event, and check that the close event is triggered by OpenLP closing the display.
        On Windows this event can be triggered by pressing ALT+F4, which we want to ignore.

        :param event: The triggered event
        """
        if self.close_display:
            super().closeEvent(event)
        else:
            event.ignore()

    def close(self):
        """
        Remove registered function on close.
        """
        if self.is_live:
            if is_macosx():
                # Block signals so signal we are disconnecting can't get called while we disconnect it
                self.blockSignals(True)
                if self.screens.current['primary']:
                    self.application.focusWindowChanged.disconnect()
                self.blockSignals(False)
            Registry().remove_function('live_display_hide', self.hide_display)
            Registry().remove_function('live_display_show', self.show_display)
            Registry().remove_function('update_display_css', self.css_changed)
        self.close_display = True
        super().close()

    def set_transparency(self, enabled):
        """
        Set the transparency of the window

        :param enabled: Is transparency enabled
        """
        if enabled:
            self.setAutoFillBackground(False)
            self.setStyleSheet(TRANSPARENT_STYLESHEET)
        else:
            self.setAttribute(QtCore.Qt.WA_NoSystemBackground, False)
            self.setStyleSheet(OPAQUE_STYLESHEET)
        self.setAttribute(QtCore.Qt.WA_TranslucentBackground, enabled)
        self.repaint()

    def css_changed(self):
        """
        We need to rebuild the CSS on the live display.
        """
        for plugin in self.plugin_manager.plugins:
            plugin.refresh_css(self.frame)

    def retranslateUi(self):
        """
        Setup the interface translation strings.
        """
        self.setWindowTitle(translate('OpenLP.MainDisplay', 'OpenLP Display'))

    def setup(self):
        """
        Set up and build the output screen
        """
        self.log_debug('Start MainDisplay setup (live = {islive})'.format(islive=self.is_live))
        self.screen = self.screens.current
        self.setVisible(False)
        Display.setup(self)
        self.web_view.setHtml(build_html_display(self.screen, plugins=self.plugin_manager.plugins))
        if self.is_live:
            self._hide_mouse()
            self.set_background_transitions()
            self.show_logoscreen()

    def text(self, slide, animate=True):
        """
        Add the slide text from slideController

        :param slide: The slide text to be displayed
        :param animate: Perform transitions if applicable when setting the text
        """
        if self.current_raw_frame == slide:
            return
        self.current_raw_frame = slide
        # Wait for the webview to update before displaying text.
        while not self.web_loaded:
            self.application.process_events()
        self.setGeometry(self.screen['size'])
        text_html = build_html_text(self.service_item, slide)
        self.show_next_frame(text_html)

    def alert(self, text, location):
        """
        Display an alert.

        :param text: The text to be displayed.
        :param location: Where on the screen is the text to be displayed
        """
        # First we convert <>& marks to html variants, then apply
        # formattingtags, finally we double all backslashes for JavaScript.
        text_prepared = expand_tags(html.escape(text)).replace('\\', '\\\\').replace('\"', '\\\"')
        if self.height() != self.screen['size'].height() or not self.isVisible():
            shrink = True
            js = 'show_alert("{text}", "{top}")'.format(text=text_prepared, top='top')
        else:
            shrink = False
            js = 'show_alert("{text}", "")'.format(text=text_prepared)
        height = self.frame.evaluateJavaScript(js)
        if shrink:
            if text:
                alert_height = int(height)
                self.resize(self.width(), alert_height)
                self.setVisible(True)
                if location == AlertLocation.Middle:
                    self.move(self.screen['size'].left(), (self.screen['size'].height() - alert_height) // 2)
                elif location == AlertLocation.Bottom:
                    self.move(self.screen['size'].left(), self.screen['size'].height() - alert_height)
            else:
                self.setVisible(False)
                self.setGeometry(self.screen['size'])
        # Workaround for bug #1531319, should not be needed with PyQt 5.6.
        if is_win():
            self.shake_web_view()

    def show_background_image(self, path, source):
        """
        API for replacement backgrounds so Images are added directly to cache.

        :param path: Path to Image
        :param source: The image source
        :type source: ImageSource
        :return True if image added to screen successfully
        """
        if self.current_background == path and self.current_background_type == 'image':
            return
        self.current_background = path
        self.current_background_type = 'image'
        image = self.image_manager.get_image_bytes(path, source)
        image_html = build_html_image(image)
        if type(self.controller) is 'SlideController':
            self.controller.media_controller.media_reset(self.controller)
        self.show_next_frame(image_html, True)
        return True

    def image(self, path):
        """
        Add an image as the background. The image has already been added to the
        cache.

        :param path: The path to the image to be displayed. **Note**, the path is only passed to identify the image.
            If the image has changed it has to be re-added to the image manager.
        """
        if self.current_raw_frame == path:
            return
        self.current_raw_frame = path
        image = self.image_manager.get_image_bytes(path, ImageSource.ImagePlugin)
        self.controller.media_controller.media_reset(self.controller)
        self.display_image(image)

    def display_image(self, image):
        """
        Display an image, as is.

        :param image: The image to be displayed
        """
        self.setGeometry(self.screen['size'])
        if image:
            image_html = build_html_image(image)
        else:
            image_html = ''
        self.show_next_frame(image_html)

    def reset_image(self):
        """
        Reset the background image to the service item image. Used after the image plugin has changed the background.
        """
        # clear the cache
        self.override = {}
        if hasattr(self, 'service_item'):
            self.show_background(self.service_item)
        else:
            self.hide_background()
        # Update the preview frame.
        self.wait_show_complete()
        if self.is_live:
            self.live_controller.update_preview()

    def preview(self):
        """
        Generates a preview of the image displayed.
        """
        was_visible = self.isVisible()
        self.application.process_events()
        # We must have a service item to preview.
        if self.is_live and hasattr(self, 'service_item') and not self.hide_mode:
            # Wait for the fade to finish before geting the preview.
            # Important otherwise preview will have incorrect text if at all!
            if self.service_item.theme_data and self.service_item.theme_data.display_slide_transition:
                # Workaround for bug #1531319, should not be needed with PyQt 5.6.
                if is_win():
                    fade_shake_timer = QtCore.QTimer(self)
                    fade_shake_timer.setInterval(25)
                    fade_shake_timer.timeout.connect(self.shake_web_view)
                    fade_shake_timer.start()
                self.wait_show_complete()
                # Workaround for bug #1531319, should not be needed with PyQt 5.6.
                if is_win():
                    fade_shake_timer.stop()
        # Wait for the webview to update before getting the preview.
        # Important otherwise first preview will miss the background !
        while not self.web_loaded:
            self.application.process_events()
        # if was hidden keep it hidden
        if self.is_live:
            if self.hide_mode:
                self.hide_display(self.hide_mode)
            # Only continue if the visibility wasn't changed during method call.
            elif was_visible == self.isVisible():
                # Single screen active
                if self.screens.display_count == 1:
                    # Only make visible if setting enabled.
                    if Settings().value('core/display on monitor'):
                        self.setVisible(True)
                else:
                    self.setVisible(True)
        # Workaround for bug #1531319, should not be needed with PyQt 5.6.
        if is_win():
            self.shake_web_view()
        return self.grab()

    def show_background(self, service_item):
        """
        Store the service_item and build the new HTML from it. Add the HTML to the display

        :param service_item: The Service item to be used
        """
        self.initial_fame = None
        self.service_item = service_item

        theme_name = service_item.theme_data.theme_name

        if self.current_background_type == 'video':
            Registry().execute('video_background_replaced')
        if self.current_background_type == 'theme' and self.current_background != theme_name:
            Registry().execute('live_theme_changed')

        if service_item.theme_data.background_type == 'image':
            self.show_background_image(self.service_item.theme_data.background_filename, ImageSource.Theme)
        elif service_item.theme_data.background_type == 'video':
            video_path = service_item.theme_data.background_filename
            self.show_background_video(video_path)
        else:
            self.show_background_theme(theme_name)

        if self.is_live:
            self.set_transitions(self.service_item)
        if service_item.foot_text:
            self.footer(service_item.foot_text)
        else:
            self.footer('')
        # if was hidden keep it hidden
        if self.hide_mode and self.is_live and not service_item.is_media():
            if Settings().value('core/auto unblank'):
                Registry().execute('slidecontroller_live_unblank')
            else:
                self.hide_display(self.hide_mode)
        self._hide_mouse()

    def show_background_theme(self, theme_name):
        if self.current_background == theme_name and self.current_background_type == 'theme':
            return
        self.current_background = theme_name
        self.current_background_type = 'theme'
        if self.service_item.theme_data.background_type == BackgroundType.to_string(BackgroundType.Transparent):
            background_html = ''
        else:
            background_html = build_html_background(self.service_item, self.screen)
        self.show_next_frame(background_html, True)
        return True

    def show_background_video(self, filename):
        """
        API for replacement video in background. Player class must control background frame visibility.

        :param filename: full path to video file
        :return True if video added to media controller successfully
        """

        if self.current_background == filename and self.current_background_type == 'video':
            return
        self.current_background = filename
        self.current_background_type = 'video'
        (path, name) = os.path.split(filename)
        service_item = ServiceItem()
        service_item.title = 'background video'
        service_item.processor = UiStrings().Automatic
        service_item.volume = 0
        service_item.add_from_command(path, name, ':/media/slidecontroller_multimedia.png')
        if self.is_live:
            return self.media_controller.video(DisplayControllerType.Live, service_item, 0, video_behind_text=True)
        else:
            return self.media_controller.video(DisplayControllerType.Preview, service_item, 0, video_behind_text=True)

    def set_background_transitions(self):
        """
        set transitions for changing background frames

        """
        transitions = Settings().value('themes/background transitions')
        speed = Settings().value('themes/background transition speed')
        self.frame.evaluateJavaScript('set_transition(\'' + transitions + '\',true, ' + str(speed) + ');')

    def set_transitions(self, item):
        """
        set transitions for changing screen frames

        :param item: Service Item to be displayed
        """
        theme_data = item.theme_data
        if self.is_live and theme_data and theme_data.display_slide_transition:
            speed = theme_data.display_slide_transition_speed
            self.frame.evaluateJavaScript('set_transition(\'' + theme_data.display_slide_transition + '\',false, ' +
                                          str(speed) + ');')
        else:
            self.frame.evaluateJavaScript('set_transition(\'None\',false, 0);')

    def wait_show_complete(self):
        """
        Wait until all animations complete on screen
        """
        start = datetime.now()
        while not self.frame.evaluateJavaScript('show_text_completed();') and (datetime.now() - start).seconds < 10:
            self.application.process_events()

    def hide_background(self):
        """
        Hide background frame with transitions if set

        :param text: footer text to be displayed
        """
        self.show_next_frame('', True)

    def footer(self, text):
        """
        Display the Footer

        :param text: footer text to be displayed
        """
        footer_html = build_html_footer(self.service_item, self.screen, text)
        self.frame.findFirstElement('#footer').setInnerXml(footer_html)

    def hide_display(self, mode=HideMode.Screen):
        """
        Hide the display by making all layers transparent Store the images so they can be replaced when required

        :param mode: How the screen is to be hidden
        """
        self.log_debug('hide_display mode = {mode:d}'.format(mode=mode))
        if self.screens.display_count == 1:
            # Only make visible if setting enabled.
            if not Settings().value('core/display on monitor'):
                return
        if self.hide_mode == mode:
            return
        if mode == HideMode.Screen:
            self.hide_content()
            self.hide_background()
            if hasattr(self, 'service_item'):
                self.footer('')
            self.wait_show_complete()
            self.setVisible(False)
        elif mode == HideMode.Blank or self.initial_fame:
            self.show_blackscreen()
            self.wait_show_complete()
        else:
            self.hide_content()
        if mode != HideMode.Screen:
            if self.isHidden():
                self.setVisible(True)
                self.web_view.setVisible(True)
            if self.hide_mode == HideMode.Screen and hasattr(self, 'service_item'):
                self.current_background = None
                self.show_background(self.service_item)
            # Workaround for bug #1531319, should not be needed with PyQt 5.6.
            if is_win():
                self.shake_web_view()
        self.hide_mode = mode

    def show_blackscreen(self):
        """
        Show black layer on top of all other layers
        """
        black_html = '<div class="frame" id="black"></div>'
        self.show_next_frame(black_html)

    def show_logoscreen(self):
        """
        Show logo image layer on top of all other layers
        """
        # Build the initial frame.
        background_color = QtGui.QColor()
        background_color.setNamedColor(Settings().value('core/logo background color'))
        if not background_color.isValid():
            background_color = QtCore.Qt.white
        image_file = Settings().value('core/logo file')
        self.current_raw_frame = image_file
        splash_image = QtGui.QImage(image_file)
        self.initial_fame = QtGui.QImage(
            self.screen['size'].width(),
            self.screen['size'].height(),
            QtGui.QImage.Format_ARGB32_Premultiplied)
        painter_image = QtGui.QPainter()
        painter_image.begin(self.initial_fame)
        painter_image.fillRect(self.initial_fame.rect(), background_color)
        painter_image.drawImage(
            (self.screen['size'].width() - splash_image.width()) // 2,
            (self.screen['size'].height() - splash_image.height()) // 2,
            splash_image)
        image = image_to_byte(self.initial_fame)
        self.display_image(image)

    def hide_content(self):
        """
        Hides current content on screen: text, images.
        Background frames are not hidden.
        """
        self.show_next_frame('')

    def show_display(self):
        """
        Show the stored layers so the screen reappears as it was originally.
        Make the stored images None to release memory.
        """
        if self.screens.display_count == 1:
            # Only make visible if setting enabled.
            if not Settings().value('core/display on monitor'):
                return
        if self.hide_mode:
            current_raw_frame = self.current_raw_frame
            self.current_raw_frame = None
        hide_mode = self.hide_mode
        self.hide_mode = None
        if hasattr(self, 'service_item'):
            if self.service_item.foot_text:
                self.footer(self.service_item.foot_text)
        if hide_mode == HideMode.Screen:
            self.current_background = None
            if hasattr(self, 'service_item') and not self.service_item.is_media():
                self.show_background(self.service_item)
        if hide_mode:
            if current_raw_frame and hasattr(self, 'service_item'):
                if self.service_item.is_text():
                    self.text(current_raw_frame)
                elif self.service_item.is_image():
                    self.image(current_raw_frame)
                else:
                    self.show_next_frame('')
            else:
                self.show_next_frame('')
        # Check if setting for hiding logo on startup is enabled.
        # If it is, display should remain hidden, otherwise logo is shown. (from def setup)
        if self.isHidden() and not Settings().value('core/logo hide on startup'):
            self.setVisible(True)
        # Trigger actions when display is active again.
        if self.is_live:
            Registry().execute('live_display_active')
            # Workaround for bug #1531319, should not be needed with PyQt 5.6.
            if is_win():
                self.shake_web_view()

    def _hide_mouse(self):
        """
        Hide mouse cursor when moved over display.
        """
        if Settings().value('advanced/hide mouse'):
            self.setCursor(QtCore.Qt.BlankCursor)
            self.frame.evaluateJavaScript('document.body.style.cursor = "none"')
        else:
            self.setCursor(QtCore.Qt.ArrowCursor)
            self.frame.evaluateJavaScript('document.body.style.cursor = "auto"')

    def change_window_level(self, window):
        """
        Changes the display window level on Mac OS X so that the main window can be brought into focus but still allow
        the main display to be above the menu bar and dock when it in focus.

        :param window: Window from our application that focus changed to or None if outside our application
        """
        if is_macosx():
            if window:
                # Get different window ids' as int's
                try:
                    window_id = window.winId().__int__()
                    main_window_id = self.main_window.winId().__int__()
                    self_id = self.winId().__int__()
                except:
                    return
                # If the passed window has the same id as our window make sure the display has the proper level and
                # collection behavior.
                if window_id == self_id:
                    self.pyobjc_nsview.window().setLevel_(NSMainMenuWindowLevel + 2)
                    self.pyobjc_nsview.window().setCollectionBehavior_(NSWindowCollectionBehaviorManaged)
                # Else set the displays window level back to normal since we are trying to focus a window other than
                # the display.
                else:
                    self.pyobjc_nsview.window().setLevel_(0)
                    self.pyobjc_nsview.window().setCollectionBehavior_(NSWindowCollectionBehaviorManaged)
                    # If we are trying to focus the main window raise it now to complete the focus change.
                    if window_id == main_window_id:
                        self.main_window.raise_()

    def shake_web_view(self):
        """
        Resizes the web_view a bit to force an update. Workaround for bug #1531319, should not be needed with PyQt 5.6.
        """
        self.web_view.setGeometry(0, 0, self.width(), self.height() - 1)
        self.web_view.setGeometry(0, 0, self.width(), self.height())


class AudioPlayer(OpenLPMixin, QtCore.QObject):
    """
    This Class will play audio only allowing components to work with a soundtrack independent of the user interface.
    """
    position_changed = QtCore.pyqtSignal(int)

    def __init__(self, parent):
        """
        The constructor for the display form.

        :param parent:  The parent widget.
        """
        super(AudioPlayer, self).__init__(parent)
        self.player = QtMultimedia.QMediaPlayer()
        self.playlist = QtMultimedia.QMediaPlaylist(self.player)
        self.volume_slider = None
        self.player.setPlaylist(self.playlist)
        self.player.positionChanged.connect(self._on_position_changed)

    def __del__(self):
        """
        Shutting down so clean up connections
        """
        self.stop()

    def _on_position_changed(self, position):
        """
        Emit a signal when the position of the media player updates
        """
        self.position_changed.emit(position)

    def set_volume_slider(self, slider):
        """
        Connect the volume slider to the media player
        :param slider:
        """
        self.volume_slider = slider
        self.volume_slider.setMinimum(0)
        self.volume_slider.setMaximum(100)
        self.volume_slider.setValue(self.player.volume())
        self.volume_slider.valueChanged.connect(self.set_volume)

    def set_volume(self, volume):
        """
        Set the volume of the media player

        :param volume:
        """
        self.player.setVolume(volume)

    def reset(self):
        """
        Reset the audio player, clearing the playlist and the queue.
        """
        self.stop()
        self.playlist.clear()

    def play(self):
        """
        We want to play the file so start it
        """
        self.player.play()

    def pause(self):
        """
        Pause the Audio
        """
        self.player.pause()

    def stop(self):
        """
        Stop the Audio and clean up
        """
        self.player.stop()

    def add_to_playlist(self, file_names):
        """
        Add another file to the playlist.

        :param file_names:  A list with files to be added to the playlist.
        """
        if not isinstance(file_names, list):
            file_names = [file_names]
        for file_name in file_names:
            self.playlist.addMedia(QtMultimedia.QMediaContent(QtCore.QUrl.fromLocalFile(file_name)))

    def next(self):
        """
        Skip forward to the next track in the list
        """
        self.playlist.next()

    def go_to(self, index):
        """
        Go to a particular track in the list

        :param index: The track to go to
        """
        self.playlist.setCurrentIndex(index)
        if self.player.state() == QtMultimedia.QMediaPlayer.PlayingState:
            self.player.play()
