# OpenLP fork

Open Source Lyrics Projection

Official website: https://openlp.org

Developer website: https://openlp.io

## This fork

Designed to provide better video support for OpenLP.

## Features

#### Video features:

* Video playlist (multi video files as single item) can be added to schedule
* Video items can loop and stored this mode in service
* Allows to store media (audio and video) volume in service
* VLC video player can play background video and playlists
* System Player can play background video and playlists
* Remove everything hardcoded for WebkitPlayer to play in background to use real settings from provided Players.
* WebkitPlayer is depricated now and won't support playlists
* Removed awful looping system that play item again by QTimer in UI of OpenLP

#### New transition system:

HTML Display is never reloading now. Display has 2 levels of frames: background and content level. Each level has 2 frames that change each other with transition effects.

* Transition sets in theme file
* Several effects are implemented:
  * __Fade-in-Fade-out__ the same as original effect in OpenLP
  * __Cross fade__
  * __Fade-in__
  * __Move-left__
  * __Move-right__
  * __Move-up__
  * __Move-down__
* Each effect can customized with speed of transition
* Transition can be set for background frames in __Preference -> Theme__
* Transitions can be used for __Black screen__, replacing background with image or video, for showing Desktop.
* Refactored code to make the control of Display levels more obvious in API
* Service item with media and images (not only text) can store theme settings to use it for animation effects.

### Action at the end

New concept to have an action at the end of playing the item.

Each service item can one of:
* __Play loop__ - repeat this service item in loop
* __Play next item__ - after playing current item to the end and send to Live next item from schedule. _Not fully implemented yet_
* __Do nothing__ - by default when adding media content to schedule
